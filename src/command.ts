/// <reference path="./query.d.ts"/>

import merge from 'lodash/merge';
import { Observable, Subject, empty } from 'rxjs';
import assert from 'assert';
import { takeUntil } from 'rxjs/operators';
import { rateLimit } from 'rxjs-util';
import { getLogger } from 'log4js';

const disposed = new Subject<true>();
if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		disposed.next( true );
		disposed.complete();
	} );
} else {
	disposed.complete();
}

export const registeredCommands = [] as RegisteredCommand<any, string>[];

const logger = getLogger( 'command' );

export function parseCommands
<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
( options: ParseCommandOptions<TParameterTypeMap, TParameterNames> ) {
	const { lifecycle } = options;
	if( lifecycle.isShutdown ) return empty();
	return new Observable<Command<TParameterTypeMap, TParameterNames>>( observer => {
		const subscriber = {
			dispatch: ( val: any, err?: Error ) => {
				if( err != null ) {
					logger.error( err );
				}
				if( lifecycle.isShutdown ) {
					observer.error( new Error( 'dispatched command after shutdown' ) );
				} else if( err == null ) {
					logger.info( val );
					observer.next( val );
				}
			},
			options: merge( {}, options )
		};
		registeredCommands.push( subscriber );
		return () => {
			const index = registeredCommands.indexOf( subscriber );
			assert.ok( index >= 0 );
			registeredCommands.splice( index, 1 );
		};
	} ).pipe(
		rateLimit( 30 ),
		takeUntil( merge( lifecycle.shutdown$, disposed ) )
	);
}

export namespace query {
	export namespace source {
		export function inThread( ...tids: readonly number[] ) {
			return {
				type: { $eq: 'mention' },
				...( tids.length > 0 ? {
					tid: {
						$in: [ ...tids ]
					}
				} : {} )
			} as Query<CommandSource>;
		}

		export function inChat( ...roomIds: readonly number[] ) {
			return {
				type: { $eq: 'chat' },
				...( roomIds.length > 0 ? {
					roomId: {
						$in: [ ...roomIds ]
					}
				} : {} )
			} as Query<CommandSource>;
		}
	}

	export function inChat( ...roomIds: readonly number[] ) {
		return {
			source: source.inChat( ...roomIds )
		} as Query<RawCommand>;
	}

	export function inThread( ...tids: readonly number[] ) {
		return {
			source: source.inThread( ...tids )
		} as Query<RawCommand>;
	}

	export namespace issuer {
		export function fromConsole() {
			return {
				type: { $eq: 'console' },
			} as Query<CommandIssuer>;
		}

		export function fromUser( ...queries: readonly Query<CommandIssuerUser>[] ) {
			return {
				$and: [
					{ type: { $eq: 'user' } },
					...queries
				]
			} as Query<CommandIssuer>;
		}

		export function fromRole( ...roles: readonly string[] ) {
			return { roles: { $in: [ ...roles ] } };
		}
	}

	export function fromConsole() {
		return {
			issuer: issuer.fromConsole()
		} as Query<RawCommand>;
	}

	export function fromUser( ...queries: readonly Query<CommandIssuerUser>[] ) {
		return {
			issuer: issuer.fromUser( ...queries )
		} as Query<RawCommand>;
	}

	export function fromRole( ...roles: readonly string[] ) {
		return {
			issuer: issuer.fromRole( ...roles )
		} as Query<RawCommand>;
	}

	export namespace raw {
		export function notMatches( regex: RegExp ) {
			return { $not: { $regex: regex } } as Query<string>;
		}

		export function matches( regex: RegExp ) {
			return { $regex: regex } as Query<string>;
		}
	}

	export function rawMatches( regex: RegExp ) {
		return { raw: raw.matches( regex ) } as Query<RawCommand>;
	}

	export function rawNotMatches( regex: RegExp ) {
		return { raw: raw.notMatches( regex ) } as Query<RawCommand>;
	}
}

