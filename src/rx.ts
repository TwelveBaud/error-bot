import { filter, retryWhen, tap, switchMapTo } from 'rxjs/operators';
import { Observable, pipe, timer } from 'rxjs';
import { Logger } from 'log4js';

export const filterType = <T extends { type: string }, KTagValue extends T['type']> ( value: KTagValue ) => filter(
	( obj: T ): obj is Extract<T, { type: KTagValue }> =>
		obj.type === value
);

export const fromAsyncIterator = <T>( value: AsyncIterableIterator<T> ) => new Observable<T>( observer => {
	let done = false;
	( async () => {
		try {
			for await( const v of value ) {
				if( done ) break;
				observer.next( v );
			}
			observer.complete();
		} catch( ex ) {
			observer.error( ex );
		}
	} )();
	return () => {
		done = true;
	};
} );


export interface HandleErrorOptions {
	readonly logger: Logger|null;
	readonly retryDelay?: number;
}
export const handleErrors = <T>( { logger, retryDelay = 1000 }: HandleErrorOptions ) => pipe(
	retryWhen<T>( err =>
		err.pipe(
			tap( e => {
				try {
					logger?.error( e );
				} catch( ex ) {
					console.error( ex );
					console.error( e );
				}
			} ),
			switchMapTo( timer( retryDelay ) )
		)
	)
);
