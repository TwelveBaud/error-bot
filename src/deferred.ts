/** @description A Promise that can be resolved or rejected externally. */
export class Deferred<T> extends Promise<T> {
	public constructor( fn?: ( resolve: ( value?: T | PromiseLike<T> ) => void, reject: ( reason?: any ) => void ) => void ) {
		let resolve: ( value: T | PromiseLike<T> ) => void;
		let reject: ( reason?: any ) => void;
		super( ( resolveFn, rejectFn ) => {
			resolve = resolveFn;
			reject = rejectFn;
			if( typeof fn === 'function' ) fn( resolve, reject );
		} );
		this.resolve = resolve;
		this.reject = reject;
	}

	public static from<T>( value: Eventually<T> ) {
		return new Deferred<T>().resolveWith( Promise.resolve( value ) );
	}

	public resolveWith( prom: PromiseLike<T> ) {
		prom.then( v => { this.resolve( v ); }, e => { this.reject( e ); } );
		return this;
	}

	public readonly resolve: ( value: T | PromiseLike<T> ) => void;
	public readonly reject: ( reason?: any ) => void;
}
