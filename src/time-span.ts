import { duration } from 'moment';

export class TimeSpan {
	private constructor( private readonly ms: number ) {}

	public static readonly Infinity = new TimeSpan( Infinity );
	public static readonly Zero = new TimeSpan( 0 );

	public static fromMilliseconds( milliseconds: number ) {
		return new TimeSpan( milliseconds );
	}

	public static fromSeconds( seconds: number ) {
		return new TimeSpan( seconds * 1000 );
	}

	public static fromMinutes( minutes: number ) {
		return new TimeSpan( minutes * 60 * 1000 );
	}

	public static fromHours( hours: number ) {
		return new TimeSpan( hours * 60 * 60 * 1000 );
	}

	public static fromDays( days: number ) {
		return new TimeSpan( days * 24 * 60 * 60 * 1000 );
	}

	public add( ts: TimeSpan ) {
		return new TimeSpan( this.ms + ts.ms );
	}

	public sub( ts: TimeSpan ) {
		return new TimeSpan( this.ms - ts.ms );
	}

	public fromDate( date: Date ) {
		return new Date( ( +date ) + this.ms );
	}

	public fromNow() {
		return this.fromDate( new Date );
	}

	public get days() { return this.ms / ( 24 * 60 * 60 * 1000 ); }
	public get hours() { return this.ms / ( 60 * 60 * 1000 ); }
	public get minutes() { return this.ms / ( 60 * 1000 ); }
	public get seconds() { return this.ms / 1000; }
	public get milliseconds() { return this.ms; }

	public valueOf() {
		return this.ms;
	}

	public toString() {
		return ( duration( this.ms, 'ms' ) as any ).format( 'y [years], d [days], h [hours], m [minutes], s [seconds], ms [milliseconds]', { trim: 'all' } );
	}
}
