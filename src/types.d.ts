declare module '~src/modules/*';

declare type Observable<T> = import( 'rxjs' ).Observable<T>;
declare type Observer<T> = import( 'rxjs' ).Observer<T>;
declare type Subject<T> = import( 'rxjs' ).Subject<T>;
declare type BehaviorSubject<T> = import( 'rxjs' ).BehaviorSubject<T>;
declare type NodeBBSession = import( '~nodebb/session' ).NodeBBSession;
declare type NodeBBSocket = import( '~nodebb/socket' ).NodeBBSocket;

declare type ArrayType<T> = T extends readonly ( infer U )[] ? U : never;
declare type ArrayTypeOrType<T> = T extends readonly ( infer U )[] ? U : T;
declare type ArrayTypeOrTypeArray<T> = T extends readonly ( infer U )[] ? U|readonly U[] : T|readonly T[];

declare type Arr<T> = T extends ReadonlyArray<any> ? ArrayType<T>|readonly ArrayType<T>[] : T;

declare type f = Arr<number[]>;


declare type Func<T, U> = ( value: T ) => U;
declare type Eventually<T = {}> = Promise<T>|T;
declare type Action<T> = Func<T, Eventually>;
declare type Factory<T, U = void> = Func<U, T>;
declare type Predicate<T> = Func<T, boolean>;
declare type FalsyValue = false|0|''|void;

declare type DictionaryOf<TKeys extends keyof any, U> = {
	readonly [ TKey in TKeys ]: U;
};

declare type Dictionary<U> = DictionaryOf<keyof any, U>;

declare type NoProperties<TKey extends keyof any> = Partial<{
	readonly [ key in TKey ]?: never;
}>;

declare type NoPropertiesOf<TType extends object> = NoProperties<keyof TType>;

declare type NoOverlap<TType1 extends object, TType2 extends object> =
	( TType1 & NoPropertiesOf<TType2> ) | ( TType2 & NoPropertiesOf<TType1> );

declare type PickOnly<TType, TKey extends keyof TType> =
	Pick<TType, TKey> &
	NoProperties<Exclude<keyof TType, TKey>>;

interface CacheEntry<T> {
	readonly expires: number;
	readonly data: T;
}

declare interface ResponseRouteBase<T extends string> {
	readonly type: T;
}

declare interface ResponseRouteThread extends ResponseRouteBase<'thread'> {
	readonly tid: number|string;
	readonly pid?: number|string;
}

declare interface ResponseRouteChat extends ResponseRouteBase<'chat'> {
	readonly roomId: number|string;
}

declare interface ResponseRouteConsole extends ResponseRouteBase<'console'> {}

declare interface ResponseRouteMinecraft extends ResponseRouteBase<'minecraft'> {}

declare type ResponseRoute = ResponseRouteThread|ResponseRouteChat|ResponseRouteConsole|ResponseRouteMinecraft;
declare type ResponseRouteType = ResponseRoute['type'];

declare interface CommandSourceBase<TTypeName extends string> {
	readonly type: TTypeName;
	readonly hash: string;
	readonly content: string;
}

declare interface CommandSourceMention extends CommandSourceBase<'mention'> {
	readonly cid: number;
	readonly tid: number;
	readonly pid: number;
}

declare interface CommandSourceChat extends CommandSourceBase<'chat'> {
	readonly roomId: string;
}

declare interface CommandSourceConsole extends CommandSourceBase<'console'> {}

declare interface CommandSourceMinecraft extends CommandSourceBase<'minecraft'> {}

declare type CommandSource = CommandSourceChat|CommandSourceMention|CommandSourceMinecraft;
declare type CommandSourceType = CommandSource['type'];

declare interface RawCommand {
	readonly timestamp: number;
	readonly issuer: CommandIssuer;
	readonly source: CommandSource;
	readonly responseRoute: ResponseRoute;
	readonly raw: string;
}

declare interface CommandParameterInfo<TName extends string, TTypeName extends CommandParameterType> {
	readonly name: TName;
	readonly type: TTypeName;
	readonly value: CommandParameterTypeMap[TTypeName];
	readonly supplied: boolean;
	readonly raw: string;
}

declare interface CommandBase<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> extends RawCommand {
	readonly name?: string;
	readonly parameterInfo: {
		readonly [ TParameterName in TParameterNames ]: CommandParameterInfo<TParameterName, TParameterTypeMap[TParameterName]>;
	};
	readonly parameters: {
		readonly [ TParameterName in TParameterNames ]: CommandParameterTypeMap[TParameterTypeMap[TParameterName]];
	};
}

declare type Command<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> =
	CommandBase<TParameterTypeMap, TParameterNames>

declare interface CommandIssuerBase<TTypeName extends string> {
	readonly type: TTypeName;
	readonly hash: string;
	readonly roles: readonly string[];
}

declare interface UserInfo {
	readonly uid: number;
	readonly username: string;
	readonly userslug: string;
	readonly groups: readonly string[];
	readonly roles: readonly string[];
}

declare interface CommandIssuerUser extends CommandIssuerBase<'user'>, UserInfo {}

declare interface CommandIssuerConsole extends CommandIssuerBase<'console'> {}

declare interface CommandIssuerModule extends CommandIssuerBase<'module'> {
	readonly name: ModuleName;
}

declare interface CommandIssuerMinecraft extends CommandIssuerBase<'minecraft'> {
	readonly username: string;
}

declare type CommandIssuer = CommandIssuerUser|CommandIssuerConsole|CommandIssuerModule|CommandIssuerMinecraft;
declare type CommandIssuerType = CommandIssuer['type'];

declare interface MessageResponse {
	readonly responseRouter: ResponseRoute;
	readonly content: string;
}

declare type CommandFilter = Query<RawCommand>;

declare interface ParseCommandOptions<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> {
	readonly command: CommandDefinition<TParameterTypeMap, TParameterNames>;
	readonly lifecycle: import( '~lifecycle' ).Lifecycle;
	readonly processParameters?: Func<string, string>;
	readonly filter?: CommandFilter;
	readonly normalize?: Partial<NormalizeOptions>;
	readonly priority?: number;
}

declare interface CommandDefinitionBase<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> {
	readonly name?: string;
	readonly prefix?: readonly string[];
	readonly query?: Query<RawCommand>;
	readonly parameters?: TParameterTypeMap extends never|void ? 'ignore' : { readonly [ TParameterName in TParameterNames ]: Extract<CommandParameterDefinition, { type: TParameterTypeMap[ TParameterName ]; }>; };
}

declare type CommandDefinition<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> =
	CommandDefinitionBase<TParameterTypeMap, TParameterNames>;

declare interface RegisteredCommand<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; }, TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string> {
	dispatch( command: Command<any, any> ): void;
	dispatch( _: void, err: Error ): void;
	readonly options: ParseCommandOptions<TParameterTypeMap, TParameterNames>;
}

declare interface CommandParameterDefinitionBase<TTypeName extends CommandParameterType> {
	readonly type: TTypeName;
	readonly conflicts?: readonly string[];
	readonly alias?: readonly string[];
	readonly position?: number;
	readonly required?: boolean;
	readonly default?: CommandParameterTypeMap[ TTypeName ]|( () => Eventually<CommandParameterTypeMap[ TTypeName ]> );
	readonly query?: Query<CommandParameterInfo<string, TTypeName>>;
}

declare interface CommandParameterTypeMap {
	onehot: boolean;
	boolean: boolean;
	enum: string;
	bigint: bigint;
	number: number;
	integer: number;
	string: string;
	rest: string;
	url: import( 'url' ).URL;
	user: UserInfo;
	vector2: [ number, number ];

}
declare type CommandParameterType = keyof CommandParameterTypeMap;

declare interface CommandParameterDefinitionBoolean extends CommandParameterDefinitionBase<'boolean'> {}
declare interface CommandParameterDefinitionOneHot extends CommandParameterDefinitionBase<'onehot'> {
	readonly groupKey: number|string;
	readonly allowNone?: boolean;
}
declare interface CommandParameterDefinitionEnum extends CommandParameterDefinitionBase<'enum'> {
	readonly values: readonly string[];
}
declare interface CommandParameterDefinitionBigInt extends CommandParameterDefinitionBase<'bigint'> {}
declare interface CommandParameterDefinitionNumber extends CommandParameterDefinitionBase<'number'> {}
declare interface CommandParameterDefinitionInteger extends CommandParameterDefinitionBase<'integer'> {}
declare interface CommandParameterDefinitionString extends CommandParameterDefinitionBase<'string'> {}
declare interface CommandParameterDefinitionRest extends CommandParameterDefinitionBase<'rest'> {}
declare interface CommandParameterDefinitionUrl extends CommandParameterDefinitionBase<'url'> {}
declare interface CommandParameterDefinitionUser extends CommandParameterDefinitionBase<'user'> {}
declare interface CommandParameterDefinitionVector2 extends CommandParameterDefinitionBase<'vector2'> {}

declare type CommandParameterDefinition =
	CommandParameterDefinitionBoolean|
	CommandParameterDefinitionOneHot|
	CommandParameterDefinitionEnum|
	CommandParameterDefinitionBigInt|
	CommandParameterDefinitionNumber|
	CommandParameterDefinitionInteger|
	CommandParameterDefinitionString|
	CommandParameterDefinitionRest|
	CommandParameterDefinitionUrl|
	CommandParameterDefinitionUser|
	CommandParameterDefinitionVector2;

declare type Enumerable<T> = readonly T[]|Iterable<T>;

declare module '!!buffer-loader!*' {
	declare const buffer;
	export default buffer;
}
