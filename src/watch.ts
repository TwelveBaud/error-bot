import { Observable, fromEvent } from 'rxjs';
import chokidar from 'chokidar';

export function watch( paths: string|string[] ) {
 	return new Observable( observer => {
		const watcher = chokidar.watch( paths, {
			ignorePermissionErrors: true,
			ignoreInitial: true,
			atomic: true,
			awaitWriteFinish: true
		} );

		const sub =
		fromEvent( watcher, 'all' )
		.subscribe( observer );

 		return () => {
			sub.unsubscribe();
			watcher.close();
		 };
 	} );
}
