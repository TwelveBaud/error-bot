export interface OptionalUnitsOptions {
	unit?: string;
}
export function optionalUnits( value: number, { unit = 'px' } = {} as OptionalUnitsOptions ) {
	if( value === 0 ) return String( value );
	else return `${value}${unit}`;
}

export namespace transform {
	export interface TranslateOptions {
		unit?: string;
	}
	export function translate( x: number, y: number, { unit } = {} as TranslateOptions ) {
		if( x === 0 && y === 0 ) return '';
		if( x === 0 ) return `transform:translateY(${optionalUnits( y, { unit } )});`;
		if( y === 0 ) return `transform:translateX(${optionalUnits( x, { unit } )});`;
		return `transform:translate(${optionalUnits( x, { unit } )},${optionalUnits( y, { unit } )});`;
	}
}
