/// <reference path="./lifecycle.d.ts"/>

import { v4 as uuid } from 'uuid';
import { Fsm } from './fsm';
import { Logger } from 'log4js';
import { shareReplay, distinctUntilChanged, startWith, map, filter, take, mapTo, takeUntil, switchMap, concat } from 'rxjs/operators';
import { getLogger } from 'log4js';
import { timer, empty, of } from 'rxjs';

export interface LifecycleParams {
	readonly name: string;
	readonly logger?: Logger;
	readonly timeout?: number|false;
}

export class Lifecycle {
	public constructor( params: LifecycleParams ) {
		this.name = params.name;
		const state$ = this.state$;
		this.logger = params.logger ?? getLogger( this.name );
		// state$.subscribe( state => {
		// 	logger.info( `${name} State: ${state}` );
		// } );
		// this.fsm.transitions.subscribe( transition => {
		// 	logger.info( `${name} Transition: ${transition}` );
		// } );
		const timeout = params.timeout ?? ( 60 * 1000 );
		if( timeout ) {
			state$.pipe(
				switchMap( state =>
					[ 'starting', 'shutting-down' ].includes( state )
					? timer( timeout )
					: empty()
				),
				takeUntil( this.disposed$ )
			).subscribe( () => {
				this.timeout();
			} );
		}
	}

	private readonly fsm = new Fsm<LifecycleFsmStates, LifecycleFsmTransitions>( {
		initial: 'starting',
		states: {
			starting: {
				transitions: {
					error: 'shutting-down',
					timeout: 'shutting-down',
					ready: 'running'
				}
			},
			running: {
				transitions: {
					error: 'shutting-down',
					done: 'shutting-down'
				}
			},
			'shutting-down': {
				transitions: {
					error: 'disposed',
					timeout: 'disposed',
					done: 'disposed'
				}
			},
			disposed: {
				final: true
			}
		},
		transitions: {
			shutdown: 'shutting-down'
		}
	} );

	public ready() {
		this.fsm.transition( 'ready' );
	}

	public shutdown() {
		this.fsm.transition( 'shutdown' );
	}

	public error( err: string|Error ) {
		this.logger.error( err ?? 'error' );
		this.fsm.transition( 'error' );
	}

	public done() {
		this.fsm.transition( 'done' );
	}

	public timeout() {
		this.logger.error( 'timeout' );
		this.fsm.transition( 'timeout' );
	}

	public readonly name: string;
	private readonly logger: Logger;
	public readonly id = uuid();
	public get state() { return this.fsm.state; }
	public readonly state$ = this.fsm.states.pipe(
		map( ( { enter } ) => enter ),
		startWith( this.state ),
		distinctUntilChanged(),
		shareReplay( { refCount: true, bufferSize: 1 } )
	);
	public get isShutdown() { return [ 'disposed', 'shutting-down' ].includes( this.state ); }
	public get isDisposed() { return this.state === 'disposed'; }
	public readonly shutdown$ = this.state$.pipe(
		filter( state => [ 'disposed', 'shutting-down' ].includes( state ) ),
		concat( of( true ) ),
		take( 1 ),
		mapTo( true as true ),
		shareReplay( { refCount: true, bufferSize: 1 } )
	);
	public readonly disposed$ = this.state$.pipe(
		filter( state => state === 'disposed' ),
		concat( of( true ) ),
		take( 1 ),
		mapTo( true as true ),
		shareReplay( { refCount: true, bufferSize: 1 } )
	);
}
