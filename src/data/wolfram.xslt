<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output
		indent="yes"
		method="html"
	/>
	<xsl:template match="/">
		<details>
			<summary>Wolfram|Alpha said:</summary>
			<xsl:apply-templates select="queryresult"/>
		</details>
	</xsl:template>
	<xsl:template match="queryresult[@success='true']">
		<xsl:apply-templates select="pod|infos/info"/>
	</xsl:template>
	<xsl:template match="queryresult[@success='false']">
		<xsl:apply-templates select="didyoumeans"/>
	</xsl:template>
	<xsl:template match="didyoumeans">
		<h1>Did you mean:</h1>
		<ul>
			<xsl:apply-templates select="didyoumean"/>
		</ul>
	</xsl:template>
	<xsl:template match="didyoumean">
		<li>
			<xsl:value-of select="text()"/>
		</li>
	</xsl:template>
	<xsl:template match="pod[@error='false']">
		<div>
			<xsl:if test="@title != ''">
				<h1>
					<xsl:value-of select="@title"/>
				</h1>
			</xsl:if>
			<p>
				<xsl:apply-templates select="subpod|img|plaintext|link"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template match="info">
		<xsl:apply-templates select="img|plaintext|link"/>
	</xsl:template>
	<xsl:template match="subpod">
		<div>
			<xsl:if test="@title != ''">
				<h2>
					<xsl:value-of select="@title"/>
				</h2>
			</xsl:if>
			<p>
				<xsl:apply-templates select="img|plaintext|link"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template match="link">
		<p>
			<a rel="nofollow noopener noreferrer" target="_blank">
				<xsl:attribute name="title">
					<xsl:value-of select="@title"/>
				</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="@url"/>
				</xsl:attribute>
				<xsl:value-of select="@text"/>
			</a>
		</p>
	</xsl:template>
	<xsl:template match="plaintext[not(preceding-sibling::img)]">
		<pre>
			<xsl:value-of select="text()"/>
		</pre>
	</xsl:template>
	<xsl:template match="img">
		<xsl:copy>
			<xsl:attribute name="alt">
				<xsl:value-of select="@alt"/>
			</xsl:attribute>
			<xsl:attribute name="title">
				<xsl:value-of select="@title"/>
			</xsl:attribute>
			<xsl:attribute name="width">
				<xsl:value-of select="@width"/>
			</xsl:attribute>
			<xsl:attribute name="height">
				<xsl:value-of select="@height"/>
			</xsl:attribute>
			<xsl:attribute name="src">
				<xsl:value-of select="@src"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
