<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output
		indent="yes"
		method="html"
	/>
	<xsl:template match="/">
		<details>
			<summary>
				<xsl:text>Merriam-Webster said:</xsl:text>
			</summary>
			<xsl:apply-templates select="entry_list" />
		</details>
	</xsl:template>
	<xsl:template match="*">
		<xsl:comment>
			<xsl:value-of select="local-name()"/>
		</xsl:comment>
	</xsl:template>
	<xsl:template match="text()">
		<xsl:copy/>
	</xsl:template>
	<xsl:template match="ahw"> <!-- alternate headword -->
		<b>
			<xsl:apply-templates select="node()" />
		</b>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="aq"> <!-- author quoted -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="art[bmp]">
		<div>
			<xsl:apply-templates select="bmp|cap" />
		</div>
	</xsl:template>
	<xsl:template match="bmp">
		<a target="_blank" rel="noreferrer noopener">
			<xsl:attribute name="href">
				<xsl:text>https://www.merriam-webster.com/art/med/</xsl:text>
				<xsl:value-of select="substring-before(text(),'.bmp')"/>
				<xsl:text>.htm</xsl:text>
			</xsl:attribute>
			<img>
				<xsl:attribute name="src">
					<xsl:text>https://www.merriam-webster.com/assets/mw/static/art/dict/</xsl:text>
					<xsl:value-of select="substring-before(text(),'.bmp')"/>
					<xsl:text>.gif</xsl:text>
				</xsl:attribute>
			</img>
		</a>
	</xsl:template>
	<xsl:template match="ca"> <!-- called also -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="cat"> <!-- called also target -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="cap">
		<div>
			<xsl:apply-templates select="node()" />
		</div>
	</xsl:template>
	<xsl:template match="cl"> <!-- cognate label -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="ct"> <!-- cognate target -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="cx"> <!-- cognate cross-entry -->
		<div>
			<xsl:apply-templates select="node()" />
		</div>
	</xsl:template>
	<xsl:template match="date">
		<div>
			<xsl:text>(</xsl:text>
			<xsl:apply-templates select="node()" />
			<xsl:text>)</xsl:text>
		</div>
	</xsl:template>
	<xsl:template match="def">
		<div>
			<xsl:apply-templates select="node()" />
		</div>
	</xsl:template>
	<xsl:template match="dro"> <!-- defined run-on -->
		<xsl:apply-templates select="drp" />
	</xsl:template>
	<xsl:template match="drp"> <!-- defined run-on phrase -->
		<xsl:apply-templates select="drp" />
	</xsl:template>
	<xsl:template match="dt[not(@mode)]|dt[@mode='dig']"> <!-- defining text -->
		<xsl:apply-templates select="node()|@*" />
		<br/>
	</xsl:template>
	<xsl:template match="dx"> <!-- directional cross-ref -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="dxn"> <!-- directional cross-ref number -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="dxt"> <!-- directional cross-ref target -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="dx_def"> <!-- directional corss-ref/definition -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="dx_ety"> <!-- directional cross-ref/etymology -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="d_link">
		<span>
			<xsl:apply-templates select="node()" />
		</span>
	</xsl:template>
	<xsl:template match="entry">
		<div>
			<xsl:apply-templates select="node()" />
		</div>
		<hr/>
	</xsl:template>
	<xsl:template match="entry_list">
		<xsl:apply-templates select="entry" />
		<xsl:if test="suggestion">
			<h1>Did you mean:</h1>
			<ul>
				<xsl:apply-templates select="suggestion" />
			</ul>
		</xsl:if>
	</xsl:template>
	<xsl:template match="suggestion">
		<li>
			<xsl:apply-templates select="node()" />
		</li>
	</xsl:template>
	<xsl:template match="et"> <!-- etymology -->
		<div>
			<xsl:apply-templates select="node()" />
		</div>
	</xsl:template>
	<xsl:template match="ew">
		<h1>
			<xsl:apply-templates select="node()" />
		</h1>
	</xsl:template>
	<xsl:template match="fw">
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="g">
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="if"> <!-- inflected form -->
		<div>
			<xsl:apply-templates select="node()" />
		</div>
	</xsl:template>
	<xsl:template match="in"> <!-- inflection field -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="it"> <!-- inflection label -->
		<b>
			<xsl:apply-templates select="node()" />
		</b>
	</xsl:template>
	<xsl:template match="fl"> <!-- functional label -->
		<xsl:text> </xsl:text>
		<i>
			<xsl:apply-templates select="node()" />
		</i>
	</xsl:template>
	<xsl:template match="hw"> <!-- head word -->
		<b>
			<xsl:apply-templates select="node()" />
		</b>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="ma">
		<i>
			<xsl:apply-templates select="node()" />
		</i>
	</xsl:template>
	<xsl:template match="pl">
		<i>
			<xsl:apply-templates select="node()" />
		</i>
	</xsl:template>
	<xsl:template match="pr[not(@mode)]|pr[@mode='dig']"> <!-- pronunciation -->
		<xsl:text>\</xsl:text>
		<xsl:apply-templates select="node()" />
		<xsl:text>\ </xsl:text>
	</xsl:template>
	<xsl:template match="pt">
		<p>
			<xsl:apply-templates select="node()" />
		</p>
	</xsl:template>
	<xsl:template match="subj"/>
	<xsl:template match="sense">
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="set"> <!-- sense-specific etymology -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="sn"> <!-- sense number -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="snp">
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="sp"> <!-- sense-specific pronunciation -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="slb"> <!-- sense-specific label -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="sound">
		<xsl:apply-templates select="wav|wpr" />
	</xsl:template>
	<xsl:template match="vi//sc">
		<b>
			<xsl:apply-templates select="node()" />
		</b>
	</xsl:template>
	<xsl:template match="sc">
		<i>
			<xsl:apply-templates select="node()" />
		</i>
	</xsl:template>
	<xsl:template match="sc[following-sibling::*[1][self::sc]]">
		<xsl:text>, </xsl:text>
	</xsl:template>
	<xsl:template match="ssl"> <!-- sense subject/status label -->
		<i>
			<xsl:text>(</xsl:text>
			<xsl:apply-templates select="node()" />
			<xsl:text>)</xsl:text>
		</i>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="svr"> <!-- sense-specific variant field -->
		<xsl:apply-templates select="node()|@*" />
	</xsl:template>
	<xsl:template match="sx"> <!-- synonymous cross-ref target -->
		<xsl:text> </xsl:text>
		<xsl:apply-templates select="node()" />
	</xsl:template>
	<xsl:template match="sxn"> <!-- synonymous cross-ref sense number -->
		<sup>
			<xsl:apply-templates select="node()" />
		</sup>
	</xsl:template>
	<xsl:template match="un"> <!-- usage note -->
		<xsl:apply-templates select="vi" />
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="ure"> <!-- undefined run-on entry -->
		<xsl:apply-templates select="vr|in|pr|fl|lb|sl" />
	</xsl:template>
	<xsl:template match="uro"> <!-- undefined run-on -->
		<xsl:apply-templates select="ure" />
	</xsl:template>
	<xsl:template match="va"> <!-- variant form -->
		<b>
			<xsl:apply-templates select="node()" />
		</b>
	</xsl:template>
	<xsl:template match="vi"> <!-- verbal illustration -->
		<xsl:text> - </xsl:text>
		<i>
			<xsl:apply-templates select="node()" />
		</i>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="vl"> <!-- variant label -->
		<xsl:apply-templates select="node()" />
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="vr"> <!-- variant spelling -->
		<xsl:apply-templates select="vl|va|pr" />
	</xsl:template>
	<xsl:template match="vt"> <!-- verb divider -->
		<xsl:apply-templates select="vi|vt" />
	</xsl:template>
	<xsl:template match="wav">
		<a target="_blank">
			<xsl:attribute name="href" rel="noreferrer noopener">
				<xsl:text>https://media.merriam-webster.com/soundc11/</xsl:text>
				<xsl:value-of select="substring( text(), 1, 1 )"/>
				<xsl:text>/</xsl:text>
				<xsl:value-of select="text()"/>
			</xsl:attribute>
			<xsl:text>:speaker_medium_volume:</xsl:text>
		</a>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="wpr">
		<xsl:apply-templates select="node()|@*" />
		<xsl:text>; </xsl:text>
	</xsl:template>
</xsl:stylesheet>
