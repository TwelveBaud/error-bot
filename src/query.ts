/// <reference path="./query.d.ts"/>

import assert from 'assert';

export const QUERY_IGNORE = Symbol( 'QUERY_IGNORE' );

export type QueryableValue<T> =
	T
	|typeof QUERY_IGNORE
	|( T extends object
		? { readonly [ TKey in keyof T ]?: T[TKey]|typeof QUERY_IGNORE }
		: never
	);

function isQueryLiteral<T>( query: QueryValue<T> ): query is QueryLiteral {
	if( typeof query !== 'object' ) return false;
	const keys1 = [ '$value' ];
	const keys2 = Object.keys( query );
	return keys1.some( k1 => keys2.includes( k1 ) );
}

function isQueryLogical<T>( query: QueryValue<T> ): query is QueryLogical<T> {
	if( typeof query !== 'object' ) return false;
	const keys1 = [ '$and', '$not', '$or', '$nor' ];
	const keys2 = Object.keys( query );
	return keys1.some( k1 => keys2.includes( k1 ) );
}

function isQueryCondition<T>( query: QueryValue<T> ): query is QueryCondition<T> {
	if( typeof query !== 'object' ) return false;
	const keys1 = [ '$eq', '$gt', '$gte', '$in', '$lt', '$lte', '$ne', '$nin', '$isNullish', '$isTruthy', '$isFalsy', '$isArray', '$isNaN', '$isFinite', '$isInteger', '$isApproximately', '$type', '$regex', '$where', '$all', '$elemMatch', '$size' ];
	const keys2 = Object.keys( query );
	return keys1.some( k1 => keys2.includes( k1 ) );
}

function isQueryValue<T>( query: QueryValue<T> ): query is QueryValue<T> {
	return isQueryLiteral( query ) || isQueryCondition( query ) || isQueryLogical( query );
}

const comparer = new Intl.Collator( 'en-US', {
	sensitivity: 'base',
	usage: 'search'
} );

function isEqual<T>( q1: T, q2: T ) {
	if( q1 == null ) return q2 == null;
	if( Array.isArray( q2 ) ) {
		if( !Array.isArray( q1 ) ) return false;
		if( q1.length !== q2.length ) return false;
		return q2.every( ( v, i ) => isEqual( v, q1[ i ] ) );
	}
	if( typeof q1 === 'object' ) {
		if( typeof q2 === 'object' ) {
			const k1 = Object.entries( q1 );
			if( Object.entries( q2 ).length !== k1.length ) return false;
			for( const [ key, value ] of k1 ) {
				if( !isEqual( value, q2[ key ] ) ) return false;
			}
		} else {
			return false;
		}
	}
	if( typeof q1 === 'string' ) {
		if( typeof q2 === 'string' ) return comparer.compare( q1, q2 ) === 0;
		else return false;
	}
	return q1 === q2;
}

function includes<T>( q1: readonly T[], q2: T ) {
	if( Array.isArray( q2 ) ) {
		return q2.some( v => includes( q1, v ) );
	}
	return q1.some( q => isEqual( q, q2 ) );
}

export function evalQueryValue<T>( value: QueryableValue<T>, query: QueryValue<T> ) {
	if( value === QUERY_IGNORE ) return true;
	assert.ok( isQueryValue( query ) );
	const keys = Object.keys( query );
	if( isQueryLiteral( query ) ) {
		assert.ok( !isQueryLogical( query ) );
		assert.ok( !isQueryCondition( query ) );
		assert.equal( typeof query.$value, 'boolean' );
		return query.$value;
	} else if( isQueryLogical( query ) ) {
		assert.ok( !isQueryCondition( query ) );
		if( keys.includes( '$and' ) ) {
			assert.ok( Array.isArray( query.$and ) );
			if( !query.$and.every( sq => evalQuery( value, sq ) ) ) return false;
		}
		if( keys.includes( '$not' ) ) {
			assert.equal( typeof query.$not, 'object' );
			if( evalQuery( value, query.$not ) ) return false;
		}
		if( keys.includes( '$nor' ) ) {
			assert.ok( Array.isArray( query.$nor ) );
			if( query.$nor.some( sq => evalQuery( value, sq ) ) ) return false;
		}
		if( keys.includes( '$or' ) ) {
			assert.ok( Array.isArray( query.$or ) );
			if( !query.$or.some( sq => evalQuery( value, sq ) ) ) return false;
		}
		return true;
	} else if( isQueryCondition( query ) ) {
		if( keys.includes( '$eq' ) ) {
			if( !isEqual( value, query.$eq ) ) return false;
		}
		if( keys.includes( '$gt' ) ) {
			if( !( value > query.$gt ) ) return false;
		}
		if( keys.includes( '$gte' ) ) {
			if( !( value >= query.$gte ) ) return false;
		}
		if( keys.includes( '$in' ) ) {
			if( !includes( Array.isArray( query.$in ) ? query.$in : [ query.$in ], value ) ) return false;
		}
		if( keys.includes( '$lt' ) ) {
			if( !( value < query.$lt ) ) return false;
		}
		if( keys.includes( '$lte' ) ) {
			if( !( value < query.$lte ) ) return false;
		}
		if( keys.includes( '$ne' ) ) {
			if( isEqual( value, query.$ne ) ) return false;
		}
		if( keys.includes( '$nin' ) ) {
			if( includes( Array.isArray( query.$nin ) ? query.$nin : [ query.$nin ], value ) ) return false;
		}
		if( keys.includes( '$isNullish' ) ) {
			assert.equal( typeof query.$isNullish, 'boolean' );
			if( !( ( value == null ) === query.$isNullish ) ) return false;
		}
		if( keys.includes( '$isTruthy' ) ) {
			assert.equal( typeof query.$isTruthy, 'boolean' );
			if( !( ( !!value ) === query.$isTruthy ) ) return false;
		}
		if( keys.includes( '$isFalsy' ) ) {
			assert.equal( typeof query.$isFalsy, 'boolean' );
			if( !( ( !value ) === query.$isFalsy ) ) return false;
		}
		if( keys.includes( '$type' ) ) {
			assert.ok( isQueryCondition( query.$type ) );
			if( !evalQueryValue( typeof value, query.$type ) ) return false;
		}
		if( keys.includes( '$isArray' ) ) {
			assert.equal( typeof query.$isArray, 'boolean' );
			if( !( Array.isArray( value ) === query.$isArray ) ) return false;
		}
		if( keys.includes( '$isNaN' ) ) {
			assert.equal( typeof query.$isNaN, 'boolean' );
			if( !( isNaN( value as any ) === query.$isNaN ) ) return false;
		}
		if( keys.includes( '$isFinite' ) ) {
			assert.equal( typeof query.$isFinite, 'boolean' );
			if( !( isFinite( value as any ) === query.$isFinite ) ) return false;
		}
		if( keys.includes( '$isInteger' ) ) {
			assert.equal( typeof query.$isInteger, 'boolean' );
			if( typeof value !== 'number' || !isFinite( value ) ) return false;
			if( !( ( value === Math.floor( value ) ) === query.$isInteger ) ) return false;
		}
		if( keys.includes( '$isApproximately' ) ) {
			assert.equal( typeof query.$isApproximately, 'number' );
			assert.ok( isFinite( query.$isApproximately ) );
			if( typeof value !== 'number' ) return false;
			if( !( Math.abs( value - query.$isApproximately ) <= Number.EPSILON ) ) return false;
		}
		if( keys.includes( '$type' ) ) {
			assert.ok( isQueryCondition( query.$type ) );
			if( !evalQueryValue( typeof value, query.$type ) ) return false;
		}
		if( keys.includes( '$type' ) ) {
			assert.ok( isQueryCondition( query.$type ) );
			if( !evalQueryValue( typeof value, query.$type ) ) return false;
		}
		if( keys.includes( '$regex' ) ) {
			let pattern: RegExp;
			if( query.$regex instanceof RegExp ) {
				const { source, flags } = query.$regex;
				pattern = new RegExp( source, flags );
			} else if( typeof query.$regex === 'string' ) {
				const firstSlash = query.$regex.indexOf( '/' );
				const lastSlash = query.$regex.lastIndexOf( '/' );
				assert.ok( firstSlash >= 0 );
				assert.ok( lastSlash > firstSlash );
				const source = query.$regex.slice( firstSlash + 1, lastSlash );
				const flags = query.$regex.slice( lastSlash + 1 );
				pattern = new RegExp( source, flags );
			} else {
				assert.fail();
			}
			if( !pattern.test( String( value ) ) ) return false;
		}
		if( keys.includes( '$where' ) ) {
			assert.ok( query.$where instanceof Function );
			if( !query.$where( value as T ) ) return false;
		}
		if( keys.includes( '$all' ) ) {
			assert.ok( isQueryValue( query.$all ) );
			if( !Array.isArray( value ) ) return false;
			if( !value.every( v => evalQuery( v, query.$all ) ) ) return false;
		}
		if( keys.includes( '$elemMatch' ) ) {
			assert.ok( isQueryValue( query.$elemMatch ) );
			if( !Array.isArray( value ) ) return false;
			if( !value.some( v => evalQuery( v, query.$elemMatch ) ) ) return false;
		}
		if( keys.includes( '$size' ) ) {
			assert.ok( isQueryCondition( query.$size ) );
			if( !( Array.isArray( value ) || typeof value === 'string' ) ) return false;
			if( !evalQueryValue( value.length, query.$size ) ) return false;
		}
		return true;
	} else {
		assert.fail();
	}
}

export function evalQueryObject<T extends object>( value: QueryableValue<T>, query: QueryObject<T> ) {
	if( value === QUERY_IGNORE ) return true;
	assert.strictEqual( typeof query, 'object' );
	for( let [ propertyName, q ] of Object.entries( query ) ) {
		switch( typeof q ) {
		case 'boolean':
		case 'number':
		case 'string':
			q = { $eq: q };
			break;
		case 'object':
			assert.ok( q != null );
			assert.ok( !Array.isArray( q ) );
			break;
		default: throw new Error( `expected ${propertyName} to be 'object', found '${typeof q}'` );
		}
		if( isQueryValue( q ) ) {
			if( !evalQueryValue( value?.[ propertyName ], q ) ) return false;
		} else if( typeof value !== 'object' ) {
			return false;
		} else if( !evalQuery( value?.[ propertyName ], q ) ) {
			return false;
		}
	}
	return true;
}

export function evalQuery<T>( value: QueryableValue<T>, query: Query<T> ) {
	if( value === QUERY_IGNORE ) return true;
	const result = isQueryValue( query ) ? evalQueryValue( value, query ) : evalQueryObject( value as any, query );
	return result;
}
