import { URL } from 'url';

import { getAgent } from '~proxy-agent';

import rp from 'request-promise';
import { userAgent } from '~data/config.yaml';
import { JSDOM } from 'jsdom';

type CookieJar = import( 'request' ).CookieJar;
type Response<T> = Omit<import( 'request' ).Response, 'body'> & { readonly body: T; };
type Body = object | Buffer | Buffer[] | string | string[] | import( 'stream' ).Readable;

interface RequestParameters {
	readonly body?: Body;
	readonly formData?: { [ key: string ]: any };
	readonly form?: { readonly [ key: string ]: any } | string;
	readonly encoding?: string;
	readonly followRedirect?: boolean;
	readonly followAllRedirects?: boolean;
	readonly headers: object;
	readonly jar?: CookieJar;
	readonly xml?: boolean;
	readonly html?: boolean;
	readonly json?: boolean;
	readonly method: string;
	readonly searchParams?: { readonly [ key: string ]: string; }
	readonly timeout?: number;
	readonly simple?: boolean;
	readonly full?: boolean;
	readonly url: string|URL;
}
async function request( { url, method, encoding, followRedirect, followAllRedirects, body, headers, simple, xml, html, json, jar, formData, form, full, timeout, searchParams }: RequestParameters ) {
	let accept = [ '*/*' ];

	if( xml ) {
		accept = [ 'application/xml', 'text/xml', ...accept ];
	} else if( html ) {
		accept = [ 'text/html', ...accept ];
	} else if( json ) {
		accept = [ 'application/json', ...accept ];
	}
	let response = await rp(
		typeof url === 'string' ? url : url.href, {
			headers: {
				Accept: accept,
				'User-Agent': userAgent,
				...headers
			},
			encoding,
			followRedirect,
			followAllRedirects,
			form,
			formData,
			method,
			body,
			agent: getAgent( url ),
			json,
			jar,
			qs: searchParams,
			timeout: timeout ?? ( 60 * 1000 ),
			simple,
			rejectUnauthorized: false,
			resolveWithFullResponse: full
		}
	);
	if( response && ( !full || response.body ) && ( xml || html ) ) {
		let responseBody: any;
		if( full ) {
			responseBody = response.body;
		} else {
			responseBody = response;
		}
		if( xml ) {
			responseBody = ( new DOMParser ).parseFromString( responseBody, 'application/xml' );
		} else if( html ) {
			responseBody = new JSDOM( responseBody );
		}
		if( full ) {
			response = { ...response, body: responseBody };
		} else {
			response = responseBody;
		}
	}
	return response;
}

interface SharedParameters {
	readonly followAllRedirects?: boolean;
	readonly followRedirect?: boolean;
	readonly headers?: object;
	readonly jar?: CookieJar;
	readonly searchParams?: { readonly [ key: string ]: string; }
	readonly simple?: boolean;
	readonly timeout?: number;
	readonly url: string|URL;
}

type IsNotNullEncoding = { readonly encoding?: string; }
type IsNullEncoding = { readonly encoding: void; } & IsNotXml & IsNotHtml & IsNotJson;

type IsFull = { readonly full: true; };
type IsNotFull = { readonly full?: false|void; };

type IsHtml = { readonly html: true; } & IsNotJson & IsNotXml & IsNotNullEncoding;
type IsNotHtml = { readonly html?: false|void; };

type IsXml = { readonly xml: true; } & IsNotJson & IsNotHtml & IsNotNullEncoding;
type IsNotXml = { readonly xml?: false|void; };

type IsJson = { readonly json: true; } & IsNotHtml & IsNotXml & IsNotNullEncoding;
type IsNotJson = { readonly json?: false|void; };

interface GetParameters extends SharedParameters {}
export function get( parameters: GetParameters & IsNotXml & IsNotHtml & IsNotJson & IsFull & IsNullEncoding ): Promise<Response<Buffer>>;
export function get( parameters: GetParameters & IsNotXml & IsNotHtml & IsNotJson & IsFull & IsNotNullEncoding ): Promise<Response<string>>;
export function get( parameters: GetParameters & IsXml & IsFull ): Promise<Response<XMLDocument>>;
export function get( parameters: GetParameters & IsHtml & IsFull ): Promise<Response<JSDOM>>;
export function get<T extends object = any>( parameters: GetParameters & IsJson & IsFull ): Promise<Response<T>>;
export function get( parameters: GetParameters & IsNotXml & IsNotHtml & IsNotJson & IsNotFull & IsNullEncoding ): Promise<Buffer>;
export function get( parameters: GetParameters & IsNotXml & IsNotHtml & IsNotJson & IsNotFull & IsNotNullEncoding ): Promise<string>;
export function get( parameters: GetParameters & IsXml & IsNotFull ): Promise<XMLDocument>;
export function get( parameters: GetParameters & IsHtml & IsNotFull ): Promise<JSDOM>;
export function get<T extends object = any>( parameters: GetParameters & IsJson & IsNotFull ): Promise<T>;
export function get( parameters: any ) {
	return request( {
		...parameters,
		method: 'GET'
	} );
}

interface PostParameters extends SharedParameters {
	readonly body?: Body;
	readonly formData?: { [ key: string ]: any };
	readonly form?: { readonly [ key: string ]: any } | string;
}
export function post( parameters: PostParameters & IsNotXml & IsNotHtml & IsNotJson & IsFull & IsNullEncoding ): Promise<Response<Buffer>>;
export function post( parameters: PostParameters & IsNotXml & IsNotHtml & IsNotJson & IsFull & IsNotNullEncoding ): Promise<Response<string>>;
export function post( parameters: PostParameters & IsXml & IsFull ): Promise<Response<XMLDocument>>;
export function post( parameters: PostParameters & IsHtml & IsFull ): Promise<Response<JSDOM>>;
export function post<T extends object = any>( parameters: PostParameters & IsJson & IsFull ): Promise<Response<T>>;
export function post( parameters: PostParameters & IsNotXml & IsNotHtml & IsNotJson & IsNotFull & IsNullEncoding ): Promise<Buffer>;
export function post( parameters: PostParameters & IsNotXml & IsNotHtml & IsNotJson & IsNotFull & IsNotNullEncoding ): Promise<string>;
export function post( parameters: PostParameters & IsXml & IsNotFull ): Promise<XMLDocument>;
export function post( parameters: PostParameters & IsHtml & IsNotFull ): Promise<JSDOM>;
export function post<T extends object = any>( parameters: PostParameters & IsJson & IsNotFull ): Promise<T>;
export function post( parameters: any ) {
	return request( {
		...parameters,
		method: 'POST'
	} );
}
