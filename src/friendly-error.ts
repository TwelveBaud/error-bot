/** @description An error suitable to be shown to the user. */
export class FriendlyError extends Error {
	public constructor( msg: string ) {
		super( msg );
	}
}
