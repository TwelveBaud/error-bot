import { parseCommands } from '~command';
import React, { Component } from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import quoteDb from '~data/quotes.yaml';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'quotes';
type Params = ModuleParamsMap[ ModuleName ];

interface QuoteProps {
	readonly avatar: ReactNode;
	readonly quotes: readonly string[];
}

interface QuoteState {
	readonly quote: string;
}

class Quote extends Component<QuoteProps, QuoteState> {
	public constructor( props: QuoteProps ) {
		super( props );
		this.state = {
			quote: props.quotes[ Math.floor( Math.random() * props.quotes.length ) ]
		};
	}

	public render() {
		const { props, state } = this;
		return (
			<blockquote>
				<cite>{props.avatar}</cite>
				&nbsp;&nbsp;
				<big>{state.quote}</big>
			</blockquote>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	for( const [ name, { aliases, avatar, quotes } ] of Object.entries( quoteDb ) ) {
		parseCommands( {
			command: {
				name: `!${name}`,
				prefix: [ name, ...aliases ]
			},
			lifecycle,
			filter: commandFilter
		} ).pipe(
			handleErrors( { logger } )
		).subscribe( () => ( {
			responseRoute
		} ) => {
			const message = renderToStaticMarkup( <Quote avatar={avatar} quotes={quotes}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} );
	}

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
