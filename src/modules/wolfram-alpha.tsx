import * as api from '~nodebb/api';
import { concatMap } from 'rxjs/operators';
import { URL } from 'url';
import { JSDOM } from 'jsdom';

import { appId } from '~data/wolfram-alpha.yaml';
import { parseCommands } from '~command';

import xsltText from '~data/wolfram.xslt';
import { xsltProcess, getParser } from 'xslt-ts';

import { extension } from 'mime-types';
import { distinct } from '~util';

import * as http from '~http';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'wolfram-alpha';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const parser = getParser();
	const xsltDoc = parser.parseFromString( xsltText, 'application/xml' );

	type WolframAlphaCommandParameters = { input: 'rest'; };
	parseCommands<WolframAlphaCommandParameters>( {
		command: {
			name: '!wolfram-alpha',
			prefix: [ 'wolfram alpha', 'w a', 'wolfram' ],
			parameters: {
				input: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( { parameters: { input }, responseRoute } ) => {
			const url = new URL( 'https://api.wolframalpha.com/v2/query' );
			url.searchParams.set( 'input', input as string );
			url.searchParams.set( 'appid', appId );
			url.searchParams.set( 'podtimeout', '30' );
			url.searchParams.set( 'formattimeout', '30' );
			url.searchParams.set( 'totaltimeout', '60' );

			const xmlDoc = await http.get( { url, xml: true } );
			const html = xsltProcess( xmlDoc, xsltDoc );
			const { window: { document } } = new JSDOM( html );
			const imgElements = Array.from( document.querySelectorAll( 'img' ) );
			const imgUrls = new Map<string, string>( ( await Promise.all(
				distinct(
					imgElements
					.map( ( { src } ) => src )
				)
				.map( async src => {
					const response = await http.get( {
						url: src,
						encoding: null,
						full: true
					} );
					const contentType = response.headers[ 'content-type' ];
					const ext = extension( contentType );
					const filename = [ new URL( src ).pathname.split( /\//g ).pop(), ext ].join( '.' );
					const url = await api.posts.upload( { session, filename, buffer: response.body, contentType } );
					return [ src, url ] as [ string, string ];
				} )
				.map( p => p.then( null, () => null ) )
			) ).filter( v => !!v ) );
			for( const img of imgElements ) {
				if( imgUrls.has( img.src ) ) {
					img.src = imgUrls.get( img.src );
				}
			}
			const message = document.body.innerHTML;
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
