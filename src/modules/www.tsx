import express from 'express';
import path from 'path';

import { createHttpTerminator } from 'http-terminator';
import { promisify } from 'util';

module.hot?.accept( [ '../components/index.ts', '../http.ts' ] );

type ModuleName = 'www';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, logger, port }: Params ) {
	module.hot?.addDisposeHandler( () => {
		lifecycle.shutdown();
	} );

	const app = express();

	app.get( /^\/picross\//, ( req, res, next ) => {
		res.set( { 'Cache-Control': `public, max-age=${60 * 60 * 24 * 365}, immutable` } );
		next();
	} );

	app.get( /\.svgz$/, ( req, res, next ) => {
		res.set( { 'Content-Encoding': 'gzip' } );
		next();
	} );

	app.use( express.static( path.join( process.cwd(), 'www' ) ) );

	const server = app.listen( port, () => {
		logger.info( `${moduleName} listening on port ${port}...` );
	} );

	const terminator = createHttpTerminator( { server } );

	lifecycle.shutdown$
	.subscribe( async () => {
		try {
			await Promise.all( [
				promisify( server.close ).call( server ),
				terminator.terminate()
			] );
			lifecycle.done();
		} catch( ex ) {
			lifecycle.error( ex );
		}
	} );
}
