/// <reference path="./webhooks.d.ts"/>

import express from 'express';

import assert from 'assert';

import * as React from 'react';

import { apiKeys } from '~data/webhooks.yaml';
import { Subject } from 'rxjs';
import { handleErrors } from '~rx';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'webhooks';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger, port, state$ }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const app = express();
	app.use( require( 'body-parser' ).json( {} ) );
	app.use( ( req, res, next ) => {
		res.setHeader( 'Connection', 'close' );
		next();
	} );

	app.use( ( req, res, next ) => {
		const apiKey = req.header( 'X-API-Key' );
		if( apiKeys.includes( apiKey ) ) {
			next();
		} else {
			res.sendStatus( 403 );
			res.end();
		}
	} );

	// const { roomId } = ( await api.modules.chats.getAll( { session, userslug: 'error_bot' } ) ).rooms.filter( room => !room.groupChat && room.users.length === 1 && room.users[ 0 ].userslug === 'error' )[ 0 ];

	const minecraftDeploy$ = new Subject<MinecraftDeployBody>();

	minecraftDeploy$.pipe(
		mergeMap( async ( { commit, commits } ) => {
			const state = state$.value ?? {};
			const lastCommit = state.minecraft?.lastCommit;
			const commitIndex = commits.findIndex( c => c.hash === commit );
			assert( commitIndex >= 0 );

			let lastCommitIndex = commits.findIndex( c => c.hash === lastCommit );
			if( lastCommitIndex < 0 ) lastCommitIndex = commits.length;

			let messageText = 'Beginning deployment:';
			let messageHtml = <p>Beginning deployment:</p>;
			if( commitIndex > lastCommitIndex ) {
				commits = commits.slice( lastCommitIndex, commitIndex );
				messageHtml = <>{messageHtml}<ul>{
					commits.map( ( { hash, message } ) =>
						<li key={hash}>
							<del>{message}</del>
						</li>
					)
				}</ul></>;
				messageText = messageText + '\n' + commits.map( ( { message } ) => '- (removed) ' + message ).join( '\n' );
			} else {
				commits = commits.slice( commitIndex, lastCommitIndex );
				messageHtml = <>{messageHtml}<ul>{
					commits.map( ( { hash, message } ) =>
						<li key={hash}>
							{message}
						</li>
					)
				}</ul></>;
				messageText = messageText + '\n' + commits.map( ( { message } ) => '- ' + message ).join( '\n' );
			}

			bus.next( {
				type: 'response',
				route: {
					type: 'minecraft'
				},
				message: messageText
			} );

			bus.next( {
				type: 'response',
				route: {
					type: 'thread',
					tid: 27157
				},
				message: renderToStaticMarkup( messageHtml )
			} );

			state$.next( {
				...state,
				minecraft: {
					...( state.minecraft ?? {} ),
					lastCommit: commit
				}
			} );
		} ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe();

	interface MinecraftDeployBody {
		readonly commit: string;
		readonly commits: readonly {
			readonly hash: string;
			readonly date: string;
			readonly message: string;
			readonly author: {
				readonly name: string;
				readonly email: string;
			};
		}[];
	}
	app.post( '/webhooks/v1/minecraft/deploy', ( req, res, next ) => {
		minecraftDeploy$.next( req.body as MinecraftDeployBody );
		res.status( 204 );
		res.end();
	} );

	const server = app.listen( port, () => {
		logger.info( `${moduleName} listening on port ${port}...` );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		minecraftDeploy$.complete();
		server.close();
		lifecycle.done();
	} );
}
