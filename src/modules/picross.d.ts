declare interface PicrossGameContext {
	readonly board: readonly ( readonly ( boolean|'x' )[] )[];
	readonly height: number;
	readonly hints: number;
	readonly history: readonly PicrossHistoryEntry[];
	readonly lastGuess: number;
	readonly roundId: string;
	readonly solution: readonly ( readonly boolean[] )[];
	readonly width: number;
}

declare interface PicrossState {
	readonly gameContext: PicrossGameContext;
	readonly savedGames: Record<string, PicrossGameContext>;
	readonly nextFile?: string;
	readonly usedGrids?: readonly string[];
}

declare type PicrossHistoryEntry = ( {
	readonly command: 'new-game';
} | {
	readonly command: 'fill'|'clear'|'mark';
	readonly square: readonly [ number, number ];
	readonly force: boolean;
} | {
	readonly command: 'auto'|'hint';
	readonly force: boolean;
} ) & { readonly issuer: CommandIssuer; };

declare type PicrossCommand = 'auto'|'fill'|'clear'|'mark'|'hint';
