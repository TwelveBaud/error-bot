import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'query';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface QueryUserCommandParameters {
		user: 'user';
	}
	parseCommands<QueryUserCommandParameters>( {
		command: {
			name: '!query user',
			prefix: [ 'query user' ],
			parameters: {
				user: {
					type: 'user',
					position: 0,
					required: true
				}
			}
		},
		lifecycle
	} )
	.subscribe( ( { parameters: { user }, responseRoute } ) => {
		const kv = Object.entries( user );
		const message = renderToStaticMarkup(
			<table>
				{ kv.map( ( [ key, value ] ) => <tr key={key}>
					<th scope="row">
						{key}
					</th>
					<td>
						{ Array.isArray( value )
						? value.map( ( v, i ) => <div key={`${key}-${i}`}>{v}</div> )
						: value
						}
					</td>
				</tr> ) }
			</table>
		);
		bus.next( {	type: 'response', message, route: responseRoute } );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
