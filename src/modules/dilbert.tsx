import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';
import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { tagUrl } from '~util';
import { ExternalContent } from '~components';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'dilbert';
type Params = ModuleParamsMap[ ModuleName ];

interface DilbertProps {
	name: string;
	url: string;
	via?: string;
	src: string;
}

class Dilbert extends PureComponent<DilbertProps> {
	public constructor( props: DilbertProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Dilbert" url={props.url} title={props.name} via={props.via}>
				<img src={props.src} alt={props.name}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type DilbertCommandParameters = { query: 'rest'; };
	parseCommands<DilbertCommandParameters>( {
		command: {
			name: '!dilbert',
			prefix: [ 'dilbert' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			let url: string;
			let via: string;

			if( /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test( query ) ) {
				url = tagUrl`https://dilbert.com/strip/${query}`;
			} else {
				switch( query ) {
				case '':
				case 'latest':
					via = url = `https://dilbert.com/`;
					break;
				default: { // search
					const searchUrl = new URL( 'https://dilbert.com/search_results' );
					searchUrl.searchParams.set( 'terms', query );
					via = url = searchUrl.href;
					break;
				} }
			}
			const { window: { document } } = await http.get( { url, html: true } );
			const img = document.querySelector( '.img-comic' ) as HTMLImageElement;
			const name = document.querySelector( '.comic-title-name' ).textContent;
			const src = new URL( img.src, url ).href;
			url = new URL( ( document.querySelector( '.img-comic-link' ) as HTMLAnchorElement ).href, url ).href;
			const message = renderToStaticMarkup( <Dilbert name={name} url={url} src={src} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
