import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands, query } from '~command';
import { Html } from '~components';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'secret';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus, tid }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface SecretCommandParameters {
		secret: 'rest';
	}
	parseCommands<SecretCommandParameters>( {
		command: {
			name: '!secret',
			prefix: [ 'secret' ],
			query: query.inChat(),
			parameters: {
				secret: {
					type: 'rest',
					required: true
				}
			}
		},
		normalize: {
			stripTags: false
		},
		lifecycle
	} )
	.subscribe( ( { parameters: { secret } } ) => {
		const message = renderToStaticMarkup(
			<div>
				<p>Somebody told me:</p>
				<blockquote>
					<Html html={secret} />
				</blockquote>
			</div>
		);
		bus.next( {
			type: 'response',
			message,
			route: { type: 'thread', tid }
		} );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
