/* eslint-disable @typescript-eslint/no-unused-vars, prefer-const */

import React from 'react';
import express from 'express';
import { fromEvent, timer } from 'rxjs';
import { Socket } from 'net';
import { takeUntil, take } from 'rxjs/operators';
import { createCanvas, loadImage } from 'canvas';
import { choose } from '~random';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';
import { tapLog } from 'rxjs-util';
import { sleep, dump, getDataUrl } from '~util';

import { Worker, MessageChannel } from 'worker_threads';

import * as api from '~nodebb/api';
import { shrinkSvg } from '~dom';

import path from 'path';
import fs from 'fs';
import glob from 'globby';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'playground';
type Params = ModuleParamsMap[ ModuleName ];

// this module is for experimentation and testing miscellaneous functionality

export default async function( { moduleName, lifecycle, bus, session, socket, tid, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}


	// const maxValue = BigInt( 10 ) ** BigInt( 10_000 );
	// interface CommandParameters {
	// 	readonly n: 'bigint';
	// 	readonly base: 'bigint';
	// }
	// parseCommands<CommandParameters>( {
	// 	command: {
	// 		prefix: [ 'persistence' ],
	// 		parameters: {
	// 			n: {
	// 				type: 'bigint',
	// 				position: 0,
	// 				required: true,
	// 				query: {
	// 					value: { $gt: BigInt( 0 ), $lte: maxValue }
	// 				}
	// 			},
	// 			base: {
	// 				type: 'bigint',
	// 				position: 1,
	// 				default: BigInt( 10 ),
	// 				query: {
	// 					value: { $gt: BigInt( 1 ), $lte: maxValue }
	// 				}
	// 			}
	// 		}
	// 	},
	// 	lifecycle
	// } )
	// .subscribe( async ( { parameters: { n, base }, responseRoute } ) => {
	// 	function getDigits( number: bigint, base: bigint ) {
	// 		let digits = [] as readonly bigint[];
	// 		while( number > 0 ) {
	// 			digits = [ number % base, ...digits ];
	// 			number = number / base;
	// 		}
	// 		return digits;
	// 	}

	// 	const maxIter = BigInt( 1_000_000_000 );
	// 	const pauseEvery = BigInt( 1_000 );
	// 	let persistence = BigInt( 0 );
	// 	for( let digits = getDigits( n, base ); digits.length > 1; digits = getDigits( digits.reduce( ( n1, n2 ) => n1 * n2, BigInt( 1 ) ), base ) ) {
	// 		++persistence;
	// 		if( ( persistence % pauseEvery ) === BigInt( 0 ) ) {
	// 			console.log( persistence );
	// 			await sleep( 1 );
	// 		}
	// 		if( lifecycle.isShutdown || persistence > maxIter ) return;
	// 	}

	// 	const message = renderToStaticMarkup( <>
	// 		{ n.toLocaleString() }
	// 		{' '}
	// 		base { base.toLocaleString() }
	// 		{' = '}
	// 		{ persistence.toLocaleString() }
	// 	</> );

	// 	bus.next( { type: 'response', message, route: responseRoute } );
	// } );

	/*
	const app = express();
	app.get( '/', ( req, res, next ) => {
		const svg = renderToStaticMarkup( <svg>
			<defs>
				<filter id="shadow" x="0" y="0">
					<feGaussianBlur result="blurOut" in="offOut" stdDeviation="8" />
				</filter>
			</defs>
			<svg x="0" y="0" width="32" height="32">

				<rect className="square"/>
				<text className="letter pencil">
					A
				</text>
				<text className="number">
					1.
				</text>
			</svg>

		</svg> );

		res.header( 'Content-Type', 'image/svg+xml' );
		res.send( svg );
		res.end();
	} );

	const port = 8081;
	const server = app.listen( port, err => {
		if( err ) logger.error( err );
		else logger.info( `${moduleName} listening on port ${port}...` );
	} );

	const connections = new Set<Socket>();
	fromEvent<Socket>( server, 'connection' )
	.pipe( takeUntil( lifecycle.shutdown$ ) )
	.subscribe( socket => {
		connections.add( socket );
		fromEvent( socket, 'close' )
		.pipe( takeUntil( lifecycle.shutdown$ ), take( 1 ) )
		.subscribe( () => {
			connections.delete( socket );
		} );
	} );

	*/

// 	const { roomId } = ( await api.modules.chats.getAll( { session, userslug: 'error_bot' } ) ).rooms.filter( room => !room.groupChat && room.users.length === 1 && room.users[ 0 ].userslug === 'error' )[ 0 ];

// 	const svg = <svg xmlns="http://www.w3.org/2000/svg">
// 		<rect x={0} y={0} width={32} height={32} fill="red" id="r"/>
// 		<script type="application/ecmascript">
// {`document.getElementById( 'r' ).setAttribute( 'fill', 'blue' );`}
// {`alert('test');`}
// 		</script>
// 	</svg>;

// 	const dataUrl = getDataUrl( 'image/svg+xml', shrinkSvg( svg ) );
// 	const message = renderToStaticMarkup( <img src={dataUrl}/> );
// 	bus.next( { type: 'response', message, route: { type: 'chat', roomId } } );

	lifecycle.shutdown$
	.subscribe( async () => {
		// server.close();
		// for( const socket of connections.values() ) {
		// 	socket.end();
		// }
		lifecycle.done();
	} );
}
