import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { get as levenshtein } from 'fast-levenshtein';
import { shuffle } from '~random';
import { ExternalContent } from '~components';
import { Html } from '~components';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'jargon';
type Params = ModuleParamsMap[ ModuleName ];

interface JargonProps {
	term: string;
	html: string;
	url: string;
}

class Jargon extends PureComponent<JargonProps> {
	public constructor( props: JargonProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="The Jargon File" url={props.url} title={props.term}>
				<Html html={props.html} baseUrl={props.url}></Html>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type JargonCommandParameters = { query: 'rest'; };
	parseCommands<JargonCommandParameters>( {
		command: {
			name: '!jargon-file',
			prefix: [ 'jargon file', 'catb', 'jargon' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			query = query.toLowerCase();
			const directoryUrl = 'http://catb.org/jargon/html/go01.html';
			const { window: { document: directoryDocument } } = await http.get( { url: directoryUrl, html: true } );
			const links = Array.from( directoryDocument.querySelectorAll( '.glossary dd dt a[href]' ) as NodeListOf<HTMLAnchorElement> );
			const comics = links.map( ( { href, textContent: term } ) => {
				const url = new URL( href, directoryUrl ).href;
				const distance = levenshtein( term.toLowerCase(), query );
				return { url, term, distance };
			} );
			const bestDistance = comics.map( c => c.distance ).sort()[ 0 ];
			const bestMatches = comics.filter( c => c.distance <= bestDistance );
			const { url, term } = shuffle( bestMatches )[ 0 ];
			const { window: { document } } = await http.get( { url, html: true } );
			const html = Array.from( document.querySelectorAll( 'dd, dt' ) ).map( n => '<div>' + n.innerHTML + '</div>' ).join( '\n' );
			const message = renderToStaticMarkup( <Jargon term={term} html={html} url={url}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
