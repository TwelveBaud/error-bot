import * as api from '~nodebb/api';
import { takeUntil, concatMap, groupBy, mergeMap } from 'rxjs/operators';
import { filterType, handleErrors } from '~rx';
import { rateLimit } from 'rxjs-util';
import striptags from 'striptags';
import { sleep } from '~util';
import { TimeSpan } from '~time-span';

type ModuleName = 'response-queue';
type Params = ModuleParamsMap[ ModuleName ];

function escapeMessage( message: string ) {
	message = message || '';
	message = message.replace( /\r+/g, '' );
	message = message.split( '\n' ).join( '\n\u00ad' );
	if( striptags( message ).replace( /\s+/gm, ' ' ).length <= 2 ) {
		message = message + '\n\u00ad\n\u00ad';
	}
	return message;
}

export default function( { moduleName, lifecycle, bus, socket, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const rateLimits = {
		console: 0,
		thread: 2000,
		chat: 1000
	} as Record<ResponseRouteType, number>;

	bus.pipe(
		filterType( 'response' ),
		groupBy( ( { route } ) => route.type ),
		mergeMap( m => m.pipe(
			rateLimit( rateLimits[ m.key ] )
		) ),
		concatMap( async ( { route, message } ) => {
			logger.debug( { route, message } );
			const unrecoverableErrors = /^\[\[error:(?:content-too-long|chat-message-too-long|invalid-data|session-invalid)/;
			switch( route.type ) {
			case 'console': {
				logger.info( message );
				break;
			};
			case 'thread': {
				const { tid, pid } = route;
				for( let i = 0; i < 3; ++i ) {
					try {
						await api.posts.reply( { socket, tid: +tid, toPid: +pid, content: escapeMessage( message ) } );
						break;
					} catch( ex ) {
						const msg = ex?.message ?? ex ?? 'error';
						logger.error( msg );
						if( unrecoverableErrors.test( msg ) ) break;
					}
					await sleep( TimeSpan.fromSeconds( 5 ) );
				}
				break;
			}
			case 'chat': {
				const { roomId } = route;
				for( let i = 0; i < 3; ++i ) {
					try {
						await api.modules.chats.send( { socket, message: escapeMessage( message ), roomId: String( roomId ) } );
						break;
					} catch( ex ) {
						const msg = ex?.message ?? ex ?? 'error';
						logger.error( msg );
						if( unrecoverableErrors.test( msg ) ) break;
					}
					await sleep( TimeSpan.fromSeconds( 5 ) );
				}
				break;
			}
			case 'minecraft': {
				bus.next( {
					type: 'minecraft-chat-send',
					message
				} );
				break;
			}
			}
		} ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
