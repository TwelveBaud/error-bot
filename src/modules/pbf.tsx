import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import { renderToStaticMarkup } from 'react-dom/server';
import React, { PureComponent } from 'react';

import { URL } from 'url';
import { ExternalContent } from '~components';

import * as http from '~http';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'pbf';
type Params = ModuleParamsMap[ ModuleName ];

interface PbfProps {
	name: string;
	url: string;
	alt: string;
	src: string;
	title: string;
	via?: string;

}

class Pbf extends PureComponent<PbfProps> {
	public constructor( props: PbfProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Perry Bible Fellowship" url={props.url} title={props.name} via={props.via}>
				<img src={props.src} alt={props.alt} title={props.title}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type PbfCommandParameters = { query: 'rest'; };
	parseCommands<PbfCommandParameters>( {
		command: {
			name: '!perry-bible-fellowship',
			prefix: [
				'perry bible fellowship',
				'p b f',
				'p b f comics'
			],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const searchUrl = new URL( 'https://pbfcomics.com/' );
			searchUrl.searchParams.set( 's', query );
			const via = searchUrl.href;
			const { window: { document: searchDocument } } = await http.get( { url: via, html: true } );
			const link = searchDocument.querySelector( '.searchthumb a[href]' ) as HTMLAnchorElement;
			if( !link ) return;
			const url = new URL( link.href, via ).href;
			const { window: { document } } = await http.get( { url, html: true } );
			const img = document.querySelector( '#comic img' ) as HTMLImageElement;
			if( !img ) return;
			const name = document.querySelector( '.pbf-comic-title' ).textContent;
			const { alt, title } = img;
			const src = new URL( img.src, url ).href;
			const message = renderToStaticMarkup( <Pbf name={name} url={url} src={src} alt={alt} title={title} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
