/* eslint-disable */

import { createCanvas } from 'canvas';
import { Fsm } from '~fsm';
import express from 'express';
import { v4 as uuid } from 'uuid';
import path from 'path';
import fs from 'fs';

import createContext from 'gl';
import { Scene, WebGLRenderer, AmbientLight, PerspectiveCamera, MeshStandardMaterial, BoxGeometry, Mesh, PointLight, Camera, Object3D, PlaneGeometry, Geometry, Material, Euler, Quaternion, BoxHelper } from 'three';
import { StopWatch } from '~stop-watch';
import { JSDOM } from 'jsdom';
import { QUERY_IGNORE, evalQuery, QueryableValue } from '~query';
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { choose } from '~random';
import { Spoiler } from '~components';
import { parseCommands, query } from '~command';
import { handleErrors } from '~rx';
import { fromEvent } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { Socket } from 'net';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../query.ts' ] );
}

type ModuleName = 'wtfery';
type Params = ModuleParamsMap[ ModuleName ];

function contextToImage( context: WebGLRenderingContext ) {
	const { canvas: { width, height } } = context;
	const pixels = new Uint8Array( width * height * 4 );
	context.readPixels( 0, 0, width, height, context.RGBA, context.UNSIGNED_BYTE, pixels );
	const canvas = createCanvas( width, height );
	const c2d = canvas.getContext( '2d' );
	const imageData = c2d.createImageData( width, height );
	imageData.data.set( pixels );
	c2d.putImageData( imageData, 0, 0 );
	const contentType = 'image/png';
	const buffer = canvas.toBuffer( contentType, {
		compressionLevel: 9
	} );
	return buffer;
}

const width = 640;
const height = 480;
const aspect = width / height;
const webGlContextAttributes = {
	alpha: true,
	antialias: true,
	depth: true,
	premultipliedAlpha: true,
	stencil: true
} as WebGLContextAttributes;

interface Disposable {
	dispose(): void;
}

function isDisposable( obj: any ): obj is Disposable {
	return typeof obj === 'object' && typeof obj.dispose === 'function';
}

function dispose( ...objects: readonly (Disposable|Object3D)[] ) {
	for( const object of objects ) {
		if( typeof ( object as Object3D ).traverse === 'function' ) {
			( object as Object3D ).traverse( o => {
				if( isDisposable( o ) ) o.dispose();
			} );
		}
		if( isDisposable( object ) ) object.dispose();
	}
}

type ArmorSlot = 'arms'|'feet'|'hands'|'head'|'legs'|'ring'|'torso';
type WeaponSlot = 'left-hand'|'right-hand'|'ranged';

function formatAlignment( [ alignment1, alignment2 ]: Alignment ) {
	if( alignment1 === Alignment1.Neutral && alignment2 === Alignment2.Neutral ) return 'true neutral';
	return [ alignment1, alignment2 ].join( ' ' );
}

const enum Alignment1 {
	Chaotic = 'chaotic',
	Neutral = 'neutral',
	Lawful = 'lawful'
}

const enum Alignment2 {
	Evil = 'evil',
	Neutral = 'neutral',
	Good = 'good'
}

type Alignment = [ Alignment1, Alignment2 ];

type CharacterVision = 'darkvision'|'vision';

const enum CharacterSize {
	Small = 'small',
	Medium = 'medium'
}

const enum CharacterRace {
	Dwarf = 'dwarf',
	Elf = 'elf',
	Gnome = 'gnome',
	Halfling = 'halfling',
	Human = 'human'
}

interface RaceInfo<TRace extends CharacterRace = CharacterRace> {
	readonly race: TRace;
	readonly abilityModifiers?: CharacterAbilityModifiers;
	readonly speed: number;
	readonly vision: CharacterVision;
	readonly size: CharacterSize;
	readonly restrictions?: {
		readonly character?: Restrictions<Omit<CharacterInfo, 'race'>>;
		readonly equipment?: Restrictions<EquipmentInfo>;
	};
}

type Misc1 = 'rusty'|'corroded'|'burnt'|'dull';
type Craftmanship = 'crude'|'mastercraft';

type ItemType = 'armor'|'clothes'|'consumable'|'jewelry'|'trinket'|'weapon';
type ArmorType = 'boots'|'coif'|'greaves'|'helmet'|'hood'|'gloves'|'ring'|'robe';
type ClothesType = never;
type ConsumableType = 'potion'|'food'|'drink';
type JewelryType = 'bracelet'|'necklace'|'ring';
type TrinketType = 'gem'|'junk';
type MeleeWeaponType = 'blade'|'blunt'|'club'|'dagger'|'hand'|'mace'|'quarterstaff'|'short-sword'|'long-sword'|'whip';
type RangedWeaponType = 'bow'|'crossbow'|'sling'|'thrown';
type WeaponType = MeleeWeaponType|RangedWeaponType;


type ItemMaterial = 'bone'|'cloth'|'gold'|'iron'|'leather'|'metal'|'mithril'|'silver'|'steel'|'stone'|'wood';
type WeightClass = 'light'|'medium'|'heavy';
type HeldItemType = 'charm'|'shield';
type EquipmentType = ArmorType|WeaponType|WeightClass|ItemMaterial;

const enum EquipmentSlot {}

interface ItemInfo {
	readonly name: string;
	readonly weight: number;
}

interface EquipmentInfo extends ItemInfo {
	readonly types: readonly EquipmentType[];
	readonly slots: readonly EquipmentSlot[];
	readonly restrictions?: Restrictions<CharacterInfo>;
	readonly abilityModifiers?: CharacterAbilityModifiers;
}

type Restrictions<T> = Query<Omit<T, 'restrictions'>>;

const itemInfos = {
	longsword: {
		name: 'long sword',
		weight: 1,
		slots: [],
		types: [ 'melee', 'weapon', 'simple', 'sword', 'longsword', 'metal', 'blade', 'steel' ]
	} as EquipmentInfo
};

const raceInfos = {
	[ CharacterRace.Dwarf ]: {
		race: CharacterRace.Dwarf,
		abilityModifiers: {
			con: 2
		},
		speed: 25,
		vision: 'darkvision',
		size: CharacterSize.Medium
	} as RaceInfo<CharacterRace.Dwarf>,
	[ CharacterRace.Elf ]: {
		race: CharacterRace.Elf,
		abilityModifiers: {
			dex: 2
		},
		speed: 30,
		vision: 'darkvision',
		size: CharacterSize.Medium
	} as RaceInfo<CharacterRace.Elf>,
	[ CharacterRace.Gnome ]: {
		race: CharacterRace.Gnome,
		abilityModifiers: {
			int: 2
		},
		speed: 25,
		vision: 'darkvision',
		size: CharacterSize.Small
	} as RaceInfo<CharacterRace.Gnome>,
	[ CharacterRace.Halfling ]: {
		race: CharacterRace.Halfling,
		abilityModifiers: {
			dex: 2
		},
		speed: 25,
		vision: 'vision',
		size: CharacterSize.Small
	} as RaceInfo<CharacterRace.Halfling>,
	[ CharacterRace.Human ]: {
		race: CharacterRace.Human,
		abilityModifiers: {
			agi: 1,
			cha: 1,
			dex: 1,
			int: 1,
			sta: 1,
			str: 1
		},
		speed: 30,
		vision: 'vision',
		size: CharacterSize.Medium
	} as RaceInfo<CharacterRace.Human>
} as {
	readonly [ TRace in CharacterRace ]: RaceInfo<TRace>;
};

function getBonus( abilityScore: number ) {
	return Math.floor( Math.max( abilityScore, 1 ) * .5 - 5 );
}

const enum CharacterClass {
	Barbarian = 'barbarian',
	Bard = 'bard',
	Cleric = 'cleric',
	Druid = 'druid',
	Fighter = 'fighter',
	Monk = 'monk',
	Paladin = 'paladin',
	Ranger = 'ranger',
	Rogue = 'rogue',
	Sorcerer = 'sorcerer',
	Warlock = 'warlock',
	Wizard = 'wizard'
}

interface ClassInfo<TClass extends CharacterClass = CharacterClass> {
	readonly class: TClass;
	readonly hitDie: number;
	readonly abilityModifiers: CharacterAbilityModifiers;
	readonly restrictions?: {
		readonly character?: Restrictions<Omit<CharacterInfo,'class'>>;
		readonly equipment?: Restrictions<EquipmentInfo>;
	}
}

namespace Restrictions {
	export namespace Equipment {
		export function armor( ...restrictions: readonly Restrictions<EquipmentInfo>[] ) {
			return { $or: [ { types: { $nin: [ 'armor' ] }, $and: [ { types: { $in: [ 'armor' ] } }, ...restrictions ] } ] };
		}

		export function armorType( ...restrictions: readonly Restrictions<readonly EquipmentType[]>[] ) {
			return armor( ...restrictions.map( types => ( { types } as Restrictions<EquipmentInfo> ) ) );
		}

		export function weapon( ...restrictions: readonly Restrictions<EquipmentInfo>[] ) {
			return { $or: [ { types: { $nin: [ 'weapon' ] }, $and: [ { types: { $in: [ 'weapon' ] } }, ...restrictions ] } ] };
		}

		export function weaponType( ...restrictions: readonly Restrictions<readonly EquipmentType[]>[] ) {
			return weapon( ...restrictions.map( types => ( { types } as Restrictions<EquipmentInfo> ) ) );
		}
	}
}

type SpellType = 'combat';

const spells = {
	'magic-missile': {

	}
};

const classInfos = {
	[ CharacterClass.Barbarian ]: {
		class: CharacterClass.Barbarian,
		hitDie: 12,
		abilityModifiers: {},
		restrictions: {
			character: {
				abilities: {
					str: { $gte: 10 }
				}
			}
		}
	} as ClassInfo<CharacterClass.Barbarian>,
	[ CharacterClass.Bard ]: {
		class: CharacterClass.Bard,
		hitDie: 8,
		abilityModifiers: {},
		restrictions: {
			character: {
				abilities: {
					cha: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $nin: [ 'heavy' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Bard>,
	[ CharacterClass.Cleric ]: {
		class: CharacterClass.Cleric,
		hitDie: 8,
		abilityModifiers: {},
		restrictions: {
			weapons: { $nin: [ 'blade' ] },
			character: {
				alignment: {
					$elemMatch: { $in: [ Alignment2.Good, Alignment2.Evil ] }
				}
			}
		}
	} as ClassInfo<CharacterClass.Cleric>,
	[ CharacterClass.Druid ]: {
		class: CharacterClass.Druid,
		hitDie: 8,
		abilityModifiers: {},
		restrictions: {
			character: {
				abilities: {
					wis: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.weaponType( { $nin: [ 'blade' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Druid>,
	[ CharacterClass.Fighter ]: {
		class: CharacterClass.Fighter,
		hitDie: 10,
		abilityModifiers: {},
		restrictions: {
			character: {
				abilities: {
					str: { $gte: 12 }
				}
			}
		}
	} as ClassInfo<CharacterClass.Fighter>,
	[ CharacterClass.Monk ]: {
		class: CharacterClass.Monk,
		abilityModifiers: {},
		hitDie: 8,
		restrictions: {
			character: {
				abilities: {
					agi: { $gte: 10 },
					str: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $in: [ 'cloth' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'hand', 'quarterstaff', 'thrown' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Monk>,
	[ CharacterClass.Paladin ]: {
		class: CharacterClass.Paladin,
		hitDie: 10,
		abilityModifiers: {},
		restrictions: {
			character: {
				alignment: { $eq: [ Alignment1.Lawful, Alignment2.Good ] }
			}
		}
	} as ClassInfo<CharacterClass.Paladin>,
	[ CharacterClass.Ranger ]: {
		class: CharacterClass.Ranger,
		hitDie: 10,
		abilityModifiers: {},
		restrictions: {
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $nin: [ 'heavy' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'blade', 'crossbow' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Ranger>,
	[ CharacterClass.Rogue ]: {
		class: CharacterClass.Rogue,
		abilityModifiers: {},
		hitDie: 8,
		restrictions: {
			character: {
				abilities: {
					dex: { $gte: 10 }
				},
				alignment: { $all: { $nin: Alignment1.Lawful } }
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $nin: [ 'heavy' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'dagger', 'short-sword', 'thrown' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Rogue>,
	[ CharacterClass.Sorcerer ]: {
		class: CharacterClass.Sorcerer,
		abilityModifiers: {},
		hitDie: 6,
		restrictions: {
			character: {
				abilities: {
					cha: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $in: [ 'cloth' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'hand', 'quarterstaff', 'thrown' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Sorcerer>,
	[ CharacterClass.Warlock ]: {
		class: CharacterClass.Warlock,
		abilityModifiers: {},
		hitDie: 8,
		restrictions: {
			character: {
				abilities: {
					cha: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $in: [ 'cloth' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'hand', 'quarterstaff', 'thrown' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Warlock>,
	[ CharacterClass.Wizard ]: {
		class: CharacterClass.Wizard,
		hitDie: 6,
		abilityModifiers: {},
		restrictions: {
			character: {
				abilities: {
					int: { $gte: 10 }
				}
			},
			equipment: {
				$and: [
					Restrictions.Equipment.armorType( { $in: [ 'cloth' ] } ),
					Restrictions.Equipment.weaponType( { $in: [ 'hand', 'quarterstaff', 'thrown' ] } )
				]
			}
		}
	} as ClassInfo<CharacterClass.Wizard>
} as {
	readonly [ TClass in CharacterClass ]: ClassInfo<TClass>;
};

interface CharacterAbilities {
	readonly agi: number;
	readonly cha: number
	readonly dex: number;
	readonly int: number;
	readonly sta: number;
	readonly str: number;
}

type CharacterAbilityModifiers = Partial<CharacterAbilities>;

function getAbilities( characterAbilities: CharacterAbilities, ...modifiers: readonly CharacterAbilityModifiers[] ) {
	return Object.fromEntries(
		Object.entries( characterAbilities )
		.map( ( [ key, value ] ) => [ key, modifiers.reduce( ( prev, curr ) => prev + ( curr[ key ] || 0 ), value as number ) ] )
	) as CharacterAbilities;
}

interface CharacterInfo {
	readonly name: string;
	readonly race: CharacterRace;
	readonly class: CharacterClass;
	readonly abilities: CharacterAbilities;
	readonly alignment: Alignment;
}

type StatusEffect = 'diseased'|'silenced'|'petrified'|'poisoned'|'paralyzed';

interface CharacterSheet {
	readonly info: CharacterInfo;
	readonly level: number;
	readonly statusEffects: ReadonlySet<StatusEffect>;
	readonly effectiveAbilities: CharacterAbilities;
	readonly hitPoints: readonly [ number, number ];
}

const enum Tier {
	T1 = 1, // common
	T2 = 2, // uncommon
	T3 = 3, // rare
	T4 = 4, // mythic
	T5 = 5  // artifact
}


interface DiceOptions {
	count?: number;
	sides?: number;
	modifier?: number;
}
function rollDice( ...options: readonly DiceOptions[] ) {
	if( options.length === 0 ) options = [ {} ];
	let result = 0;
	for( const { count = 1, sides = 6, modifier = 0 } of options ) {
		for( let i = 0; i < count; ++i ) {
			result += Math.floor( Math.random() * sides ) + 1;
		}
		result += modifier;
	}
	return result;
}

function isValidCharacter( character: Partial<CharacterInfo> ) {
	const queryable = {
		abilities: QUERY_IGNORE,
		name: QUERY_IGNORE,
		alignment: QUERY_IGNORE,
		race: QUERY_IGNORE,
		class: QUERY_IGNORE,
		...character
	} as QueryableValue<CharacterInfo>;
	if( typeof character.race === 'string' ) {
		const raceInfo = raceInfos[ character.race ];
		if( !evalQuery( queryable, ( raceInfo.restrictions || {} ).character || {} ) ) return false;
	}
	if( typeof character.class === 'string' ) {
		const classInfo = classInfos[ character.class ];
		if( !evalQuery( queryable, ( classInfo.restrictions || {} ).character || {} ) ) return false;
	}
	return true;
}

function generateCharacter() {
	let characterInfo = {
		abilities: {
			agi: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			cha: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			dex: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			int: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			sta: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			str: rollDice( { count: 2, sides: 6, modifier: 6 } ),
			wis: rollDice( { count: 2, sides: 6, modifier: 6 } )
		}
	} as Pick<CharacterInfo, 'abilities'>;

	const allRaces = Object.keys( raceInfos ) as readonly ( keyof typeof raceInfos )[];
	const allClasses = Object.keys( classInfos ) as readonly ( keyof typeof classInfos )[];
	let possibleCharacters =
		[ characterInfo ]
		.flatMap( character => allRaces.map( race => ( { ...character, race, abilities: getAbilities( character.abilities, raceInfos[ race ].abilityModifiers || {} ) } ) ) )
		.flatMap( character => allClasses.map( className => ( { ...character, class: className, abilities: getAbilities( character.abilities, classInfos[ className ].abilityModifiers || {} ) } ) ) )
		.filter( isValidCharacter );
	const charNoAlign = choose( possibleCharacters );
	const allAlignment1s = [ Alignment1.Lawful, Alignment1.Neutral, Alignment1.Chaotic ];
	const allAlignment2s = [ Alignment2.Good, Alignment2.Neutral, Alignment2.Evil ];
	const allAlignments = allAlignment1s.flatMap( a1 => allAlignment2s.map( a2 => ( [ a1, a2 ] as Alignment ) ) );
	const allCharAlignments =
		allAlignments
		.flatMap( alignment => ( { ...charNoAlign, alignment } ) )
		.filter( isValidCharacter );

	const char = { ...choose( allCharAlignments ), name: 'Soandso' };
	return char as CharacterInfo;
}

type TreasureFsmStates = '';
type TreasureFsmTransitions = 'pick lock'|'inspect'|'disarm'|'open';

function canEquip( item: EquipmentInfo, character: CharacterInfo ) {
	const classInfo = classInfos[ character.class ];
	if( !evalQuery( character, item.restrictions || {} ) ) return false;
	if( !evalQuery( item, ( classInfo.restrictions || {} ).equipment || {} ) ) return false;
	return true;
}

export default async function( { moduleName, session, socket, lifecycle, bus, tid, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	// const profiler = StopWatch.start().mark();
	// const context = createContext( width, height, { ...webGlContextAttributes } );
	// const canvas = ( new JSDOM ).window.document.createElement( 'canvas' );
	// canvas.width = width;
	// canvas.height = height;
	// Object.defineProperties( context, { canvas: { get: () => canvas, configurable: false } } );
	// const renderer = new WebGLRenderer( {
	// 	canvas,
	// 	context,
	// 	...webGlContextAttributes
	// } ); //


	type FsmStates = 'camp'|'explore'|'combat'|'game-over';
	type FsmTransitions = 'abandon'|'complete'|'death'|'encamp'|'encounter'|'escape'|'discover'|'restart'|'quit';
	const fsm = new Fsm<FsmStates, FsmTransitions>( {
		states: {
			camp: {},
			explore: {
				transitions: {
					encamp: 'camp',
					encounter: 'combat'
				}
			},
			combat: {
				transitions: {
					escape: 'explore',
					complete: 'explore'
				}
			},
			'game-over': {
				transitions: {}
			}
		},
		transitions: {
			death: 'game-over',
			quit: 'game-over'
		}
	} );

	parseCommands( {
		command: {
			prefix: [
				'step east', 'east', 'step e', 's e', 'e',
				'step west', 'west', 'step w', 's w', 'w',
				'step north', 'north', 'step n', 's n', 'n',
				'step south', 'south', 'step s', 's s', 's'
			],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
		// TODO
		bus.next( { type: 'response', message: 'You don\'t have a compass!', route: { type: 'thread', tid } } );
	} );


	parseCommands( {
		command: {
			prefix: [ 'step forward', 'forward', 'step f', 's f', 'f' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'back pedal', 'back step', 'back', 'step back', 'step b', 's b', 'b' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'left', 'turn left', 'turn l', 't l', 'l' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'strafe left', 'strafe l', 'step left', 'step l', 's l' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'right', 'turn right', 'turn r', 't r', 'r' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'strafe right', 'strafe r', 'step right', 'step r', 's r' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'set up camp', 'encamp', 'camp' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'explore' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'rest' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'camp' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'attack' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state !== 'combat' ) return;
	} );

	parseCommands( {
		command: {
			prefix: [ 'quit' ],
			query: query.inThread( tid )
		},
		lifecycle
	} )
	.pipe(
		handleErrors( { logger } )
	).subscribe( () => {
		if( fsm.state === 'game-over' ) return;

	} );

	/*


	Exploring:
		f forward
		l left, t l, turn left
		s l, strafe left
		r right, turn right
		s r, strafe right
		b back
		about face | turn around
		cast <spell>
		camp

	Camping:
		r rest
		memorize, pray
		cast <spell>

	Combat:
		attack
		cast <spell>

*/

	// profiler.mark( 'init' );

	// const scene = new Scene;
	// const light = new PointLight( 0xffffff );
	// scene.add( light );
	// light.position.set( 0, .5, 1 );

	// const camera = new PerspectiveCamera( 50, aspect, 0.1, 100 );
	// scene.add( camera );

	// const material = new MeshStandardMaterial( {
	// 	color: 0xff0000
	// } );
	// const geom = new BoxGeometry( 1, 1, 1 );
	// const mesh = new Mesh( geom, material );
	// scene.add( mesh );

	// mesh.position.set( 1, 1, 1 );
	// camera.lookAt( mesh.position );

	// profiler.mark( 'build scene' );

	// renderer.render( scene, camera );
	// profiler.mark( 'render' );

	// const image = contextToImage( context );
	// profiler.mark( 'image' );

	// const filepath = path.resolve( 'C:\\Users\\error\\Desktop\\wtfery.png' );
	// fs.writeFileSync( filepath, image );

//
	// profiler.mark( 'save' );

	const char = generateCharacter();
	logger.info( formatAlignment( char.alignment ), char.class );

	// const abilityScores = Object.entries( char.abilities );

	// const message = renderToStaticMarkup(
	// 	<>
	// 	<p>
	// 		You are <b>{char.name}</b>, level 1 {formatAlignment(char.alignment)} {char.race} {char.class}.
	// 	</p>
	// 	<Spoiler summary="Ability Scores">
	// 		<table>
	// 			{abilityScores.map( ( [ name, value ] ) => <tr key={name}>
	// 				<th scope="row">{name}</th>
	// 				<td>{value}</td>
	// 			</tr>)}
	// 		</table>
	// 	</Spoiler>
	// 	</>
	// );

	// logger.info( message );
	// bus.next( { type: 'response', message, route: { type: 'thread', tid } } );


	//
//
	// const filename = '3d.png';
	// const url = await api.posts.upload( { session, filename, buffer, contentType } );
	// const message = `![](${url})`;
	// bus.next( { type: 'response', message, route: { type: 'thread', tid } } );

//	logger.debug( profiler.toString() );

	const app = express();

	const id = uuid();

	app.get( '/id', ( req, res ) => {
		res.header( 'Content-Type', 'application/json' );
		res.send( JSON.stringify( { id } ) );
		res.end();
	} );

	app.get( '/render', ( req, res ) => {
		const profiler = StopWatch.start().mark();

		const contentType = 'image/png';
		const context = createContext( width, height, { ...webGlContextAttributes } );
		const canvas = ( new JSDOM ).window.document.createElement( 'canvas' );
		canvas.width = width;
		canvas.height = height;
		Object.defineProperties( context, { canvas: { get: () => canvas, configurable: false } } );
		const renderer = new WebGLRenderer( {
			canvas,
			context,
			...webGlContextAttributes
		} );

		const scene = new Scene;
		scene.add( new AmbientLight( 0xffffff, 0.4 ) );
		const light = new PointLight( 0xffffff );
		scene.add( light );
		light.position.set( 0, .5, 1 );

		const camera = new PerspectiveCamera( 50, aspect, 0.1, 100 );
		scene.add( camera );

		const material = {
			default:
				new MeshStandardMaterial( {
					color: 0xff00
				} )
		} as Record<string, Material>;

		const geom = {
			plane: new PlaneGeometry( 1, 1, 1, 1 )
		} as Record<string, Geometry>;

		const obj = {
			ceiling: new Mesh( geom.plane, material.default ),
			left: new Mesh( geom.plane, material.default ),
			right: new Mesh( geom.plane, material.default ),
			back: new Mesh( geom.plane, material.default ),
			floor: new Mesh( geom.plane, material.default )
		} as Record<string, Object3D>;

		obj.floor.position.set( 0, -.5, 0 );
		obj.floor.setRotationFromEuler( new Euler( Math.PI * 1.5, 0, Math.PI * .5 ) );
		obj.left.position.set( -.5, 0, 0 );
		obj.left.setRotationFromEuler( new Euler( Math.PI * 1.5, Math.PI * .5, 0 ) );
		obj.right.position.set( .5, 0, 0 );
		obj.right.setRotationFromEuler( new Euler( Math.PI * 1.5, Math.PI * 1.5, 0 ) );
		obj.back.position.set( 0, 0, .5 );
		obj.back.setRotationFromEuler( new Euler( Math.PI * 1, Math.PI * 0, Math.PI * 0 ) );
		obj.ceiling.position.set( 0, .5, 0 );
		obj.ceiling.setRotationFromEuler( new Euler( Math.PI * 1.5, Math.PI * 1, Math.PI * .5 ) );
		for( const m of Object.values( obj ) ) {
			scene.add( m );
			scene.add( new BoxHelper( m ) );
		}

		camera.position.set( 0, 0, -2 );
		camera.lookAt( camera.position.x, camera.position.y, camera.position.z + camera.far );

		profiler.mark( 'build scene' );

		renderer.render( scene, camera );
		profiler.mark( 'render' );

		const image = contextToImage( context );

		res.header( 'Content-Type', contentType );
		res.write( image );
		res.end();

		dispose( renderer, camera, scene,
			...Object.values( obj ),
			...Object.values( material ),
			...Object.values( geom )
		);
	} );

	app.get( '/', ( req, res ) => {
		const html = <html>
			<head>
				<meta charSet="utf-8"/>
				<title>Test</title>
				<script dangerouslySetInnerHTML={{ __html: `
async function checkUpdate() {
	try {
		const response = await fetch( '/id' );
		if( !response.ok ) return;
		const { id } = await response.json();
		if( id !== ${JSON.stringify( id )} ) {
			location.reload( true );
			return;
		}
	} catch( ex ) {
		console.error( ex );
	}
	setTimeout( checkUpdate, 100 );
}
setTimeout( checkUpdate, 100 );
`}}/>
			</head>
			<body>
				<img src="/render"/>
			</body>
		</html>;
		res.header( 'Content-Type', 'text/html' );
		res.send( renderToStaticMarkup( html ) );
		res.end();
	} );

	const port = 8080;
	const server = app.listen( port, () => {
		logger.info( `${moduleName} listening on port ${port}...` );
	} );

	const connections = new Set<Socket>();
	fromEvent<Socket>( server, 'connection' )
	.pipe( takeUntil( lifecycle.shutdown$ ) )
	.subscribe( socket => {
		connections.add( socket );
		fromEvent( socket, 'close' )
		.pipe( takeUntil( lifecycle.shutdown$ ), take( 1 ) )
		.subscribe( () => {
			connections.delete( socket );
		} );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		server.close();
		for( const socket of connections.values() ) {
			socket.end();
		}

		// dispose( scene, geom, material, renderer );
		// context.getExtension( 'STACKGL_destroy_context' ).destroy();
		lifecycle.done();
	} );
}
