import { concatMap } from 'rxjs/operators';
import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';
import { tagUrl } from '~util';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

interface HttpCatProps {
	readonly statusCode: number;
}
class HttpCat extends PureComponent<HttpCatProps> {
	public constructor( props: HttpCatProps ) {
		super( props );
	}

	public render() {
		const { statusCode } = this.props;
		return <img src={tagUrl`https://http.cat/${statusCode}`}/>;
	}
}

type ModuleName = 'httpcat';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface HttpCatCommandParameters {
		readonly statusCode: 'number';
	}
	parseCommands<HttpCatCommandParameters>( {
		command: {
			name: '!http-cat',
			prefix: [ 'http cat', 'http' ],
			parameters: {
				statusCode: {
					position: 0,
					alias: [ 'status', 'code' ],
					type: 'number',
					required: true,
					query: {
						value: {
							$isInteger: true,
							$gte: 100,
							$lt: 600
						}
					}
				}
			}
		},
		lifecycle
	} )
	.pipe(
		concatMap( async ( { parameters: { statusCode }, responseRoute } ) => {
			const message = renderToStaticMarkup( <HttpCat statusCode={statusCode}/> );
			bus.next( {	type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	)
	.subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
