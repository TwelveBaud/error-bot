import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { parseCommands } from '~command';
import { Uptime } from '~components';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'uptime';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus, commandFilter }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	parseCommands( {
		command: {
			name: '!uptime',
			prefix: [ 'up time' ]
		},
		lifecycle,
		filter: commandFilter
	} )
	.subscribe( ( { responseRoute } ) => {
		const message = renderToStaticMarkup( <Uptime ms={process.uptime() * 1000}/> );
		bus.next( { type: 'response', route: responseRoute, message } );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
