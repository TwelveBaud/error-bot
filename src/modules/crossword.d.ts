interface CrosswordPuzzle {
	readonly acrossmap: unknown;
	readonly admin: boolean;
	readonly answers: {
		readonly across: readonly string[];
		readonly down: readonly string[];
	};
	readonly author: string;
	readonly autowrap: unknown;
	readonly bbars: unknown;
	readonly circles: readonly ( 0|1 )[]|void;
	readonly clues: {
		readonly across: readonly string[];
		readonly down: readonly string[];
	};
	readonly code: unknown;
	readonly copyright: string;
	readonly date: string;
	readonly dow: string;
	readonly downmap: unknown;
	readonly editor: string;
	readonly grid: readonly string[];
	readonly gridnums: readonly number[];
	readonly hastitle: boolean;
	readonly hold: unknown;
	readonly id: unknown;
	readonly id2: unknown;
	readonly interpretcolors: unknown;
	readonly notepad?: string;
	readonly jnotes: string;
	readonly key: unknown;
	readonly mini: unknown;
	readonly publisher: string;
	readonly rbars: unknown;
	readonly shadecircles: void|'true'|'false';
	readonly size: {
		readonly cols: number;
		readonly rows: number;
	};
	readonly target: string;
	readonly title: string;
	readonly track: unknown;
	readonly type: unknown;
	readonly uniclue: unknown;
	readonly valid: boolean;
}

interface CrosswordRenderBlackCell {
	readonly black: true;
}

interface CrosswordRenderCell {
	readonly black?: false;
	readonly pencil?: boolean;
	readonly circle?: boolean;
	readonly highlight?: string|false;
	readonly letter: string;
	readonly number?: number|void;
}

interface CrosswordRenderParams {
	readonly size: {
		readonly cols: number;
		readonly rows: number;
	};
	readonly cells: readonly ( CrosswordRenderBlackCell|CrosswordRenderCell )[];
	readonly youWin: boolean;
}

interface CrosswordGameContext {
	readonly roundId: string;
	readonly puzzle: CrosswordPuzzle;
	readonly guessed: readonly string[];
	readonly pencil: readonly boolean[];
}

interface CrosswordState {
	readonly gameContext: CrosswordGameContext;
	readonly gameContextHistory: readonly CrosswordGameContext[];
	readonly gameContextHistoryIndex: number|null;
}

declare interface CrosswordResponse {
	readonly element: () => Promise<JSX.Element>;
	readonly route: ResponseRoute;
}

interface CrosswordSquare {
	readonly index: number;
	readonly x: number;
	readonly y: number;
	readonly number?: number;
	readonly letter: string;
	readonly guessed: string;
	readonly isBlack: boolean;
	readonly isBlank: boolean;
	readonly isCorrect: boolean;
	readonly isPencil: boolean;
	readonly isCircled: boolean;
}

type ArrayIndex<T, U> = import( '~array-index' ).ArrayIndex<T, U>;

interface CrosswordSquareSet extends ReadonlyArray<CrosswordSquare> {
	readonly byIndex: ArrayIndex<CrosswordSquare, 'index'>;
	readonly byNumber: ArrayIndex<CrosswordSquare, 'number'>;
	readonly byXY: ArrayIndex<CrosswordSquare, 'x'|'y'>;
	readonly emptyCount: number;
	readonly filledCount: number;
	readonly correctCount: number;
	readonly incorrectCount: number;
	readonly isCorrect: boolean;
	readonly isEmpty: boolean;
	readonly isFilled: boolean;
	readonly length: number;
}
