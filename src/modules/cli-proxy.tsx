import { filter, map, groupBy, mergeMap } from 'rxjs/operators';
import { connectWs } from '~ws';

import { bufferDebounceTime } from 'rxjs-util';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands, query } from '~command';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

interface TerminalProps {
	text: string;
}

class Terminal extends PureComponent<TerminalProps> {
	public constructor( props: TerminalProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<pre>{props.text}</pre>
		);
	}
}

type Params = ModuleParamsMap[ 'cli-proxy' ];

type BufferMessage = {
	name: 'stdin'|'stdout'|'stderr';
	data: string;
};

export default async function( { bus, url, tid, lifecycle, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const { wsIn, wsOut } = connectWs<BufferMessage>( { url, logger, lifecycle, retryDelay: 5000 } );

	logger.info( { url, tid } );

	// wsIn.subscribe( data => {
	// 	dump( logger, data );
	// } );

	const buffers =
	wsIn.pipe(
		groupBy( g => g.name ),
		mergeMap( g =>
			g.pipe(
				map( ( { data } ) => data ),
				bufferDebounceTime<string>( 250 ),
				map( s =>
					s.join( '' )
					.split( /\r|\n/g )
					.map( s => s.replace( /^\s+$/, '' ) )
					.join( '\n' )
					.replace( /(?:^\n+|\n+$)/g, '' )
					.replace( /\n{3,}/g, '\n\n' )
				),
				filter( s => !!s ),
				map( data => ( { name: g.key, data } ) )
			)
		)
	);

	buffers
	.subscribe( ( { name, data } ) => {
		logger.debug( `${name}> ${data}` );
	} );

	buffers.pipe(
		filter( ( { name } ) => name === 'stdout' )
	).subscribe( ( { data } ) => {
		logger.info( data );
		const message = renderToStaticMarkup( <Terminal text={data}/> );
		bus.next( { type: 'response', message, route: { type: 'thread', tid } } );
	} );

	parseCommands( {
		command: {
			prefix: [ 'any key', 'any', 'enter', 'more', 'next', 'return' ],
			parameters: 'ignore',
			query: {
				$and: [
					query.inThread( tid ),
					query.rawMatches( /^[![]/ )
				]
			}
		},
		lifecycle
	} )
	.subscribe( () => {
		wsOut.next( { name: 'stdin', data: '\n' } );
	} );

	parseCommands( {
		command: {
			prefix: [ 'space' ],
			parameters: 'ignore',
			query: {
				$and: [
					query.inThread( tid ),
					query.rawMatches( /^[![]/ )
				]
			}
		},
		lifecycle
	} )
	.subscribe( () => {
		wsOut.next( { name: 'stdin', data: ' ' } );
	} );

	interface CommandParameters {
		newline: 'boolean',
		text: 'rest';
	}
	parseCommands<CommandParameters>( {
		command: {
			query: {
				$and: [
					query.inThread( tid ),
					query.rawNotMatches( /^[![]/ )
				]
			},
			parameters: {
				newline: {
					type: 'boolean',
					alias: [ 'linebreak' ],
					default: true
				},
				text: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle
	} )
	.subscribe( ( { parameters: { text, newline } } ) => {
		wsOut.next( { name: 'stdin', data: text + ( newline ? '\n' : '' ) } );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		wsOut.complete();
		lifecycle.done();
	} );
}

