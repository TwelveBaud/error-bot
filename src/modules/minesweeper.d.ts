declare interface MinesweeperLockout {
	readonly uid: number;
	readonly username: string;
	readonly userslug: string;
	readonly expires: number;
	readonly turns?: number;
}


declare type MinesweeperHistoryEntry = ( {
	readonly command: 'new-game';
	readonly seed: string;
} | {
	readonly command: 'flag'|'reveal';
	readonly square: readonly [ number, number ];
	readonly force: boolean;
} | {
	readonly command: 'unflag';
	readonly square: readonly [ number, number ];
} | {
	readonly command: 'hint';
	readonly force: boolean;
} ) & { readonly issuer: CommandIssuer; };

declare type MinesweeperCommand = 'flag'|'unflag'|'reveal'|'hint';


declare interface MinesweeperGameContext {
	readonly flags: readonly ( readonly [ number, number ] )[];
	readonly height: number;
	readonly hints: number;
	readonly history: readonly MinesweeperHistoryEntry[];
	readonly lastGuess: number;
	readonly lockouts: readonly MinesweeperLockout[];
	readonly mines: readonly ( readonly [ number, number ] )[];
	readonly revealed: readonly ( readonly [ number, number ] )[];
	readonly roundId: string;
	readonly seed: string;
	readonly width: number;
}

declare interface MinesweeperState {
	readonly gameContext: MinesweeperGameContext;
	readonly savedGames: Record<string, Omit<MinesweeperGameContext, 'lockouts'>>;
}

declare interface MinesweeperResponse {
	readonly element: () => Promise<JSX.Element>;
	readonly route: ResponseRoute;
}

interface MinesweeperRenderParams {
	readonly size: {
		readonly cols: number;
		readonly rows: number;
	};
	readonly cells: readonly ( false|'mine'|'explosion'|'mistake'|'flag'|number )[];
	readonly showCoordinates: boolean;
}
