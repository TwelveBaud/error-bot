import React, { ReactChild } from 'react';
import { parseCommands } from '~command';
import { handleErrors } from '~rx';
import { concatMap } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';
import { Spoiler } from '~components';


type Params = ModuleParams;

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

export default async function( { bus, commandFilter, lifecycle, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const formatter = new Intl.NumberFormat( 'en-US', { useGrouping: true } );

	type Op = '+'|'-';
	interface Term {
		op: Op;
		value: number;
		count: number;
		display: ReactChild;
	}

	function parseTerm( s: string, op: Op ): Term {
		const MAX_VALUE = 256_000_000;
		s = s?.trim() ?? '';
		if( !s ) throw new Error( 'failed to parse term' );
		const constantPattern = /^([0-9]+)$/i
		const dicePattern = /^([0-9]*)d([0-9]+)$/i;
		let match: RegExpExecArray;
		const sign = op === '-' ? -1 : 1;
		if( match = constantPattern.exec( s ) ) {
			const result = parseInt( match[ 1 ], 10 );
			if( !isFinite( result ) || Math.abs( result ) > MAX_VALUE ) throw new Error( 'constant out of bounds' );
			return {
				op,
				value: result * sign,
				count: 1,
				display: formatter.format( result )
			};
		}
		if( match = dicePattern.exec( s ) ) {
			let count: number;
			if( match[ 1 ] == '' ) count = 1;
			else count = parseInt( match[ 1 ], 10 );
			if( !isFinite( count ) || count < 1 || count > 256 )  {
				throw new Error( 'dice count out of bounds' );
			}
			const sides = parseInt( match[ 2 ], 10 );
			if( !isFinite( sides ) || sides < 2 || sides > MAX_VALUE )  {
				throw new Error( 'dice sides out of bounds' );
			}
			const label = `${count}d${sides}`;
			const results = Array.from( { length: count }, () => Math.floor( Math.random() * sides ) + 1 );
			const result = results.reduce( ( x, y ) => x + y, 0 );
			return {
				op,
				value: result * sign,
				count,
				display: <>{label}{' '}({results.reduce( ( x, r, i ) => <>{x}{ i > 0 ? ', ' : ''}<i>{formatter.format(r)}</i></>, <></> )})</>
			};
		}
		throw new Error( 'failed to parse term: ' + s );
	}

	interface RollCommandParameters {
		label: 'string';
		query: 'rest';
	};
	parseCommands<RollCommandParameters>( {
		command: {
			name: '!roll',
			prefix: [ 'dice', 'die', 'roll' ],
			parameters: {
				label: {
					alias: [ 'for' ],
					type: 'string',
					required: false
				},
				query: {
					type: 'rest',
					required: false
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		concatMap( async ( { parameters: { label, query }, responseRoute } ) => {
			query = query?.trim();
			if( query == '' ) query = 'd20';
			if( !label ) {
				if( query.indexOf( ':' ) > 0 ) {
					( [ label, query ] = query.split( ':' ) );
					label = label.trim();
					query = query.trim();
				}
			}
			label = label.replace( /:$/, '' ).replace( /for\s+/i, '' );
			query = query.replace( /,/g, ' ' );
			let terms = [] as readonly Term[];
			let op: Op = '+';
			let numDice = 0;
			while( query.length > 0 ) {
				const mop = /^[-+]/.exec( query );
				op = mop?.[ 0 ] as Op ?? '+';
				query = query.slice( mop?.[ 0 ]?.length ?? 0 ).trimStart();
				const mterm = /^(?:[0-9]*d)?[0-9]+/.exec( query );
				const term = parseTerm( mterm?.[ 0 ], op );
				query = query.slice( mterm?.[ 0 ]?.length ?? 0 ).trimStart();
				terms = [ ...terms, term ];
				numDice += term.count;
				if( numDice > 256 ) throw new Error( 'term count out of bounds' );
			}
			if( numDice > 256 ) throw new Error( 'term count out of bounds' );

			let message: string;
			const prefix = <><b>{label || query}</b>{': '}</>;
			const details = terms.reduce( ( x, y, i ) =>
				<>
					{x}
					{i === 0 ? y.op === '+' ? '' : `${y.op} ` : ` ${y.op} `}
					{y.display}
				</>
			, <></> );

			const total = terms.reduce( ( x, y ) => x + y.value, 0 );
			message = renderToStaticMarkup(
				<>
					{prefix}<b>{formatter.format( total )}</b>
					<Spoiler>
						{details}
					</Spoiler>
				</>
			);
			bus.next( {
				type: 'response',
				message,
				route: responseRoute
			} );
		} ),
		handleErrors( { logger } )
	)
	.subscribe();


	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
