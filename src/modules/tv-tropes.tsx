import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { ExternalContent } from '~components';
import { Html } from '~components';

import * as http from '~http';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'tv-tropes';
type Params = ModuleParamsMap[ ModuleName ];

interface TvTropesProps {
	url: string;
	via?: string;
	titleHtml: string;
	contentHtml: string;
}

class TvTropes extends PureComponent<TvTropesProps> {
	public constructor( props: TvTropesProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="TV Tropes" url={props.url} via={props.via}>
				<h1>
					<Html html={props.titleHtml} baseUrl={props.url}></Html>
				</h1>
				<div>
					<Html html={props.contentHtml} baseUrl={props.url}></Html>
				</div>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface TvTropesCommandParameters { query: 'rest'; }
	parseCommands<TvTropesCommandParameters>( {
		command: {
			name: '!tv-tropes',
			prefix: [ 'tv tropes?', 'tropes?', 't t', 't v t', 't v', ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const searchUrl = new URL( 'https://tvtropes.org/pmwiki/elastic_search_result.php' );
			searchUrl.searchParams.set( 'new_search', 'true' );
			searchUrl.searchParams.set( 'q', query );
			searchUrl.searchParams.set( 'page_type', 'all' );
			const via = searchUrl.href;
			const { window: { document: searchDoc } } = await http.get( { url: searchUrl, html: true } );
			const bestResult = searchDoc.querySelector( '.search-result' ) as HTMLAnchorElement;
			const url = new URL( bestResult.href, searchUrl ).href;
			const { window: { document } } = await http.get( { url, html: true } );
			for( const ad of Array.from( document.querySelectorAll( '.proper-ad-unit, .mobile-ad, .footer-article-ad, script, iframe, *[id^="proper-ad"], .folder, .folderlabel, hr ~ *, h2 ~ *, .notelabel, .inlinefolder' ) ) ) {
				ad.remove();
			}
			for( const img of Array.from( document.querySelectorAll( 'img' ) ) ) {
				img.removeAttribute( 'alt' );
			}
			for( const a of Array.from( document.querySelectorAll( 'a' ) ) ) {
				if( /^\/pmwiki\//.test( a.title ) ) a.removeAttribute( 'title' );
			}
			const titleHtml = document.querySelector( '.entry-title, h1' ).innerHTML;
			const contentHtml = document.querySelector( '#main-article' ).innerHTML;
			const message = renderToStaticMarkup( <TvTropes via={via} url={url} titleHtml={titleHtml} contentHtml={contentHtml} /> );
			bus.next( { type: 'response', route: responseRoute, message } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
