/* eslint-disable @typescript-eslint/no-unused-vars */

/// <reference path="./zemu.d.ts"/>

import fs from 'fs';
import path from 'path';
import assert from 'assert';
import { takeUntil, exhaustMap, filter, take, debounceTime, map, mergeMap, tap } from 'rxjs/operators';
import { getConfig } from '~nodebb/api';
import * as rest from '~nodebb/rest';
import { timer, interval, merge, fromEvent, of } from 'rxjs';
import { parseCommands, query } from '~command';
import { handleErrors } from '~rx';

type ModuleName = 'zemu';
type Params = ModuleParamsMap[ ModuleName ];

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

function readString( buf: Buffer, offset: number, length: number ) {
	let chars = buf.slice( offset, offset + length );
	const nulIndex = chars.indexOf( 0 );
	if( nulIndex >= 0 ) {
		chars = chars.slice( 0, nulIndex );
	}
	return chars.toString( 'ascii' );
}

interface ZemuMemoryMapValue<TType> {
	get(): TType;
	set( value: TType ): void;
}

class ZemuMemoryMapValueFlag implements ZemuMemoryMapValue<boolean> {
	public constructor( private readonly value: ZemuMemoryMapFlags, private readonly index: number ) {}
	public get() { return this.value.getFlag( this.index ); }
	public set( value: boolean ) { this.value.setFlag( this.index, value ); }
	public toggle() { return this.value.toggleFlag( this.index ); }
	public valueOf() { return this.get(); }
}

interface ZemuMemoryMapFlags {
	mapFlag( index: number ): ZemuMemoryMapValueFlag;
	mapFlags(): readonly ZemuMemoryMapValueFlag[];
	getFlag( index: number ): boolean;
	setFlag( index: number, value: boolean ): void;
	toggleFlag( index: number ): boolean;
}

class ZemuMemoryMapValueUInt8 implements ZemuMemoryMapValue<number>, ZemuMemoryMapFlags {
	public constructor( private readonly memMap: ZemuMemoryMap, private readonly offset: number ) {}
	public get() { return this.memMap.readUInt8( this.offset ); }
	public set( value: number ) { this.memMap.writeUInt8( this.offset, value ); }
	public mapFlag( index: number ) { return new ZemuMemoryMapValueFlag( this, index ); }
	public mapFlags() { return Array.from( { length: 8 } ).map( ( _, i ) => new ZemuMemoryMapValueFlag( this, i ) ); }
	public getFlag( index: number ) { return ( ( this.get() >> index ) & 1 ) === 1; }
	public setFlag( index: number, value: boolean ) {
		if( value ) this.set( this.get() | ( 1 << index ) );
		else this.set( this.get() & ( 0xff ^ ( 1 << index ) ) );
	}
	public toggleFlag( index: number ) {
		const value = !this.getFlag( index );
		this.setFlag( index, value );
		return value;
	}
	public valueOf() { return this.get(); }
	public toString() { return '0x' + this.get().toString( 16 ).padStart( 2, '0' ); }
}

class ZemuMemoryMapValueUInt16 implements ZemuMemoryMapValue<number>, ZemuMemoryMapFlags {
	public constructor( private readonly memMap: ZemuMemoryMap, private readonly offset: number ) {}
	public get() { return this.memMap.readUInt16( this.offset ); }
	public set( value: number ) { this.memMap.writeUInt16( this.offset, value ); }
	public mapFlag( index: number ) { return new ZemuMemoryMapValueFlag( this, index ); }
	public mapFlags() { return Array.from( { length: 16 } ).map( ( _, i ) => new ZemuMemoryMapValueFlag( this, i ) ); }
	public getFlag( index: number ) { return ( ( this.get() >> index ) & 1 ) === 1; }
	public setFlag( index: number, value: boolean ) {
		if( value ) this.set( this.get() | ( 1 << index ) );
		else this.set( this.get() & ( 0xffff ^ ( 1 << index ) ) );
	}
	public toggleFlag( index: number ) {
		const value = !this.getFlag( index );
		this.setFlag( index, value );
		return value;
	}
	public valueOf() { return this.get(); }
	public toString() { return '0x' + this.get().toString( 16 ).padStart( 2, '0' ); }
}

class ZemuMemoryMapValueString implements ZemuMemoryMapValue<string> {
	public constructor( private readonly memMap: ZemuMemoryMap, private readonly offset: number, private readonly length ) {}
	public get() { return this.memMap.readString( this.offset, this.length ); }
	public set( value: string ) { this.memMap.writeString( this.offset, value, this.length ); }
	public valueOf() { return this.get(); }
	public toString() { return JSON.stringify( this.get() ); }
}

class ZemuMemoryMapRoutine {
	public readonly localVarCount: number;
	public constructor( private readonly memMap: ZemuMemoryMap, private readonly offset: number ) {
		this.localVarCount = memMap.readUInt8( offset );
		assert.ok( this.localVarCount < 16 );
	}
}

class ZemuMemoryMap {
	public constructor( private readonly buffer: Buffer ) {}

	public get length() { return this.buffer.byteLength; }

	public mapUInt8( offset: number ) {
		return new ZemuMemoryMapValueUInt8( this, offset );
	}

	public readUInt8( offset: number ) {
		return this.buffer.readUInt8( offset );
	}

	public mapUInt16( offset: number ) {
		return new ZemuMemoryMapValueUInt16( this, offset );
	}

	public writeUInt8( offset: number, value: number ) {
		this.buffer.writeUInt8( value, offset );
	}

	public readUInt16( offset: number ) {
		return this.buffer.readUInt16BE( offset );
	}

	public writeUInt16( offset: number, value: number ) {
		this.buffer.writeUInt16BE( value, offset );
	}

	public mapString( offset: number, length: number ) {
		return new ZemuMemoryMapValueString( this, offset, length );
	}

	public readString( offset: number, length: number ) {
		let chars = this.buffer.slice( offset, offset + length );
		const nulIndex = chars.indexOf( 0 );
		if( nulIndex >= 0 ) {
			chars = chars.slice( 0, nulIndex );
		}
		return chars.toString( 'ascii' );
	}

	public writeString( offset: number, value: string, length?: number ) {
		const buf = new Buffer( value, 'ascii' );
		if( length == null ) length = buf.byteLength;
		assert.ok( buf.byteLength <= length );
		for( let i = 0; i < length; ++i ) {
			const c = ( i >= value.length ) ? 0 : buf.readUInt8( i );
			this.buffer.writeUInt8( offset + i, c );
		}
	}

	public submap( offset: number, length: number ) {
		return new ZemuMemoryMap( this.buffer.subarray( offset, offset + length ) );
	}
}

export default function( { moduleName, lifecycle, bus, socket, session, logger, tid, zfile, state$ }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	( async () => {
		const filename = path.basename( zfile );
		const rom = await fs.promises.readFile( zfile, { encoding: null } );
		const mem = Buffer.alloc( rom.byteLength );
		rom.copy( mem, 0, 0, rom.byteLength );
		const mm = new ZemuMemoryMap( mem );
		if( lifecycle.isShutdown ) return;
		const mmHeader = mm.submap( 0, 0x40 );

		mmHeader.writeUInt16( 0x1e, 0x0101 ); // interpreter version 1.1

		const version = mmHeader.readUInt8( 0 );
		assert.ok( [ 1, 2, 3, 4, 5, 6, 7, 8 ].includes( version ) );
		if( version > 3 ) throw new Error( 'Not implemented' );

		const memAddr = mmHeader.readUInt16( 0x06 );


		const himemBase = mmHeader.readUInt16( 0x04 );
		const staticBase = mmHeader.readUInt16( 0x0e );
		const staticLen = Math.min( rom.byteLength, 0xffff );

		assert.ok( staticBase >= 0x40 );
		assert.ok( himemBase > staticBase );
		const mmDynamic = mm.submap( 0, staticBase );
		const mmStatic = mm.submap( staticBase, staticLen );
		const mmHi = mm.submap( himemBase, mm.length - himemBase );


		const flags = mmHeader.mapUInt8( 1 );
		if( version <= 3 ) {
			flags.setFlag( 4, false ); // status line unavailable
			flags.setFlag( 5, false ); // screen splitting
			flags.setFlag( 6, false ); // variable pitch font
		} else {
			flags.setFlag( 0, false ); // colors
			flags.setFlag( 1, false ); // picture
			flags.setFlag( 2, false ); // boldface
			flags.setFlag( 3, false ); // italic
			flags.setFlag( 4, true ); // fixed-space
			flags.setFlag( 5, false ); // sound effects
			flags.setFlag( 7, false ); // timed keyboard input
		}

		const stdRev = mmHeader.mapUInt8( 0x32 );

		const maxStoryLen = new Map<number, number>( [
			[ 1, 128 ],
			[ 2, 128 ],
			[ 3, 128 ],
			[ 4, 256 ],
			[ 5, 256 ],
			[ 6, 512 ],
			[ 7, 512 ],
			[ 8, 512 ]
		 ] ).get( version );

		const R_O = mmHeader.mapUInt16( 0x28 );
		const S_O = mmHeader.mapUInt16( 0x2a );
		function unpackAddr( packed: number, offset = 0 ) {
			switch( version ) {
			case 1:
			case 2:
			case 3: return packed * 2;
			case 4:
			case 5: return packed * 4;
			case 6:
			case 7: return packed * 4 + offset;
			case 8: return packed * 8;
			}
		}

		const fileLen = mmHeader.readUInt16( 0x1a ) * new Map<number, number>( [ [ 1, 2 ], [ 2, 2 ], [ 3, 2 ], [ 4, 4 ], [ 5, 5 ], [ 6, 8 ], [ 7, 8 ], [ 8, 8 ] ] ).get( version );
		if( fileLen ) {
			assert.strictEqual( fileLen, mm.length );
		}
		const flags2 = mmHeader.mapUInt16( 0x10 );
		const [ flagTranscripting, flagFixedPitch, flagScreenRedraw ] = flags2.mapFlags();

		flags2.setFlag( 3, false ); // pictures
		flags2.setFlag( 4, false ); // undo
		flags2.setFlag( 5, false ); // mouse
		flags2.setFlag( 6, false ); // colors
		flags2.setFlag( 7, false ); // sound
		flags2.setFlag( 8, false ); // menus

		const serialNum = mmHeader.mapString( 0x12, 6 );

		console.log( {
			filename,
			version,
			fileLen,
			memAddr: memAddr.toString( 16 ),
			stdRev: stdRev.toString(),
			flags: flags.toString(),
			flags2: flags2.toString(),
			himemBase: himemBase.toString(),
			staticBase: staticBase.toString(),
			serialNum: serialNum.toString()
		} );
	} )().then( null, err => {
		logger.fatal( err );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
