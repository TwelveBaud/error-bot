import { toArray } from 'rxjs/operators';
import { concatMap } from 'rxjs/operators';
import { fromAsyncIterator, handleErrors } from '~rx';
import { parseCommands } from '~command';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import * as http from '~http';

import { get as levenshtein } from 'fast-levenshtein';
import { shuffle } from '~random';
import { ExternalContent, Spoiler } from '~components';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'amazingsuperpowers';
type Params = ModuleParamsMap[ ModuleName ];

interface AmazingSuperPowersProps {
	date: string;
	name: string;
	alt: string;
	title: string;
	url: string;
	src: string;
	hiddenSrc: string;
}

class AmazingSuperPowers extends PureComponent<AmazingSuperPowersProps> {
	public constructor( props: AmazingSuperPowersProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Amazing Super Powers" title={props.name} url={props.url}>
				<p>
					<img src={props.src} alt={props.alt} title={props.title}/>
					<br/>
					<abbr title={props.title}>&shy;</abbr>
				</p>
				{ props.hiddenSrc ?
				<Spoiler summary="Hidden Comic">
					<img src={props.hiddenSrc}/>
				</Spoiler>
				: <></> }
			</ExternalContent>
		);
	}
}

async function *getComics() {
	let directoryUrl = 'https://www.amazingsuperpowers.com/category/comics/';
	for( ;; ) {
		const { window: { document } } = await http.get( { url: directoryUrl, html: true } );
		const rows = Array.from( document.querySelectorAll( '.archive-table tbody tr' ) );
		for( const row of rows ) {
			const date = row.querySelector( '.archive-date' ).textContent;
			const name = row.querySelector( '.archive-title' ).textContent;
			const url = new URL( row.querySelector( 'a' ).href, directoryUrl ).href;
			yield { name, date, url };
		}
		const nextPage = document.querySelector( '.pagenav-left a[href]' ) as HTMLAnchorElement;
		if( !nextPage ) break;
		directoryUrl = new URL( nextPage.href, directoryUrl ).href;
	}
}

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type AmazingSuperPowersCommandParameters = { query: 'rest'; };
	parseCommands<AmazingSuperPowersCommandParameters>( {
		command: {
			name: '!amazing-super-powers',
			prefix: [ 'amazing super powers', 'a s p' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const comics = (
				await fromAsyncIterator( getComics() )
				.pipe( toArray() )
				.toPromise()
			).map( ( { name, ...rest } ) => ( { name, ...rest, distance: levenshtein( name.toLowerCase(), query ) } ) );
			const bestDistance = comics.map( c => c.distance ).sort()[ 0 ];
			const bestMatches = comics.filter( c => c.distance <= bestDistance );
			const { url, date, name } = shuffle( bestMatches )[ 0 ];
			const { window: { document } } = await http.get( { url, html: true } );
			const img = document.querySelector( '#comic img' ) as HTMLImageElement;
			if( !img ) return;

			const hiddenLink = document.querySelector( '#question a[href]' ) as HTMLAnchorElement;
			const hiddenUrl = new URL( hiddenLink.href, url ).href;

			const { window: { document: hiddenDocument } } = await http.get( { url: hiddenUrl, html: true } );
			const hiddenImg = hiddenDocument.querySelector( '#comic img' ) as HTMLImageElement;
			const hiddenSrc = new URL( hiddenImg.src, hiddenUrl ).href;

			const message = renderToStaticMarkup( <AmazingSuperPowers date={date} name={name} url={url} src={img.src} alt={img.alt} title={img.title} hiddenSrc={hiddenSrc}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
