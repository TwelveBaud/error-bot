import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';
import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { tagUrl } from '~util';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'bash';
type Params = ModuleParamsMap[ ModuleName ];

interface BashProps {
	href: string;
	via?: string;
	text: string;
	id: string;
}

class Bash extends PureComponent<BashProps> {
	public constructor( props: BashProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<div>
				<p>
					<a href={props.href} rel="noopener noreferrer" target="_blank">
						bash {props.id}
					</a>
				</p>
				<pre>
					{props.text}
				</pre>
				{ props.via
				? <p>
					(via{' '}
						<a href={props.via} target="_blank" rel="noopener noreferrer">
							{props.via}
						</a>
					)
				</p> : <></> }
			</div>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type BashCommandParameters = { query: 'rest'; };
	parseCommands<BashCommandParameters>( {
		command: {
			name: '!bash',
			prefix: [ 'bash org', 'bash' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			let num: number;
			let url: string;
			let via: string;
			if( /^#?\d+$/.test( query ) ) {
				num = parseInt( query.replace( /^#/, '' ), 10 );
			}
			if( num && isFinite( num ) ) {
				url = tagUrl`http://www.bash.org/?${num}`;
			} else {
				switch( query ) {
				case '':
				case 'random':
					via = url = `http://www.bash.org/?random`;
					break;
				default:
					via = url = tagUrl`http://www.bash.org/?search=${query}&sort=0&show=1`;
				}
			}
			const { window: { document } } = await http.get( { url, html: true } );
			const link = document.querySelector( '.quote a' ) as HTMLAnchorElement;
			if( !link ) return;
			const href = new URL( link.href, url ).href;
			const id = link.textContent;
			const t = document.querySelector( '.quote + .qt' ).textContent;
			const message = renderToStaticMarkup( <Bash href={href} id={id} text={t} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
