import { proxy } from '~data/config.yaml';
import createHttpsProxyAgent from 'https-proxy-agent';

import { URL } from 'url';

export function getAgent( url: string|URL ) {
	if( !proxy ) return undefined;
	if( typeof url === 'string' ) url = new URL( url );

	let { protocol, hostname: host, port } = new URL( proxy );

	let secureProxy: boolean;
	switch( protocol ) {
	case 'http:':
		secureProxy = false;
		port = port || '80';
		break;
	case 'https:':
		secureProxy = true;
		port = port || '443';
		break;
	default: throw new Error( `Unsupported proxy: ${proxy}` );
	}
	return createHttpsProxyAgent( {
		host,
		port: +port,
		secureProxy
	} ) as any;
}
