import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';
import { Spoiler } from './spoiler';
import { duration } from 'moment';

if( module.hot ) {
	module.hot.accept( './spoiler.tsx' );
}

const validChar = /^[a-z]$/i;

export interface HangmanLockout {
	readonly username: string;
	readonly expires: number;
	readonly turns?: number;
}

export interface HangmanProps {
	readonly word: string;
	readonly guessed: readonly string[];
	readonly lockouts: readonly HangmanLockout[];
	readonly children?: ReactNode;
}

function putChar( str: readonly string[][], x: number, y: number, c: string ) {
	return str.map( ( sy, iy ) =>
		y === iy ? sy.map(
			( sx, ix ) => x === ix ? c : sx
		) : sy
	);
}

export class Hangman extends PureComponent<HangmanProps> {
	public constructor( props: HangmanProps ) {
		super( props );
	}

	public render() {
		const now = +new Date;
		const { word, guessed, lockouts } = this.props;
		const wrongGuesses = guessed.filter( c => word.indexOf( c ) < 0 );
		const wrongGuessCount = wrongGuesses.length;
		const wrongLetters = wrongGuesses.filter( c => c.length === 1 );
		const wrongWords = wrongGuesses.filter( c => c.length > 1 );

		let picture = [
			'   ╔━━━━━━╕     ',
			'   ║      │     ',
			'   ║            ',
			'   ║            ',
			'   ║            ',
			'   ║            ',
			'   ║            ',
			'╒══╬══╕     '
		].map( s => s.split( '' ) );

		let youLose = false;

		if( wrongGuessCount >= 1 ) {
			picture = putChar( picture, 9, 2, ':face_with_open_mouth:' );
		}
		if( wrongGuessCount >= 2 ) {
			picture = putChar( picture, 9, 2, ':loudly_crying_face:' );
			picture = putChar( picture, 10, 3, '┃' );
		}
		if( wrongGuessCount >= 3 ) {
			picture = putChar( picture, 9, 2, ':anxious_face_with_sweat:' );
			picture = putChar( picture, 9, 3, '─' );
			picture = putChar( picture, 10, 3, '┨' );
		}
		if( wrongGuessCount >= 4 ) {
			picture = putChar( picture, 9, 2, ':fearful_face:' );
			picture = putChar( picture, 10, 3, '╂' );
			picture = putChar( picture, 11, 3, '─' );
		}
		if( wrongGuessCount >= 5 ) {
			picture = putChar( picture, 9, 2, ':face_screaming_in_fear:' );
			picture = putChar( picture, 9, 4, '╱' );
		}
		if( wrongGuessCount >= 6 ) {
			picture = putChar( picture, 9, 2, ':skull:' );
			picture = putChar( picture, 11, 4, '╲' );
			youLose = true;
		}

		if( !youLose && word.split( '' ).every( c => !validChar.test( c ) || guessed.includes( c ) ) ) {
			picture = putChar( picture, 15, 1, ':fireworks:' );
			if( wrongGuessCount > 0 ) {
				picture = putChar( picture, 9, 2, ':grinning_face_with_big_eyes:' );
			} else {
				picture = putChar( picture, 9, 2, ':hundred_points:' );
			}
		}

		return <div>
			<pre>{ '\n' }{
				picture.map( s => s.join( '' ) ).join( '\n' )
			}{
				word.split( '' ).map( ( c, i ) => <span key={`a${i}`}>
					{( validChar.test( c ) && !guessed.includes( c ) )
					? ( youLose ? <ins>{c}</ins> : '⍰' )
					: c}{ ' ' }</span>
				)
			}{ '\n' }{
				wrongLetters.flatMap( ( letter, i ) => ( [ <del key={`b${i}`}>{letter}</del>, ' ' ] ) )
			}{ '\n' }{
				wrongWords.flatMap( ( word, i ) => ( [ <del key={`c${i}`}>{word}</del>, '\n' ] ) )
			}{ '\n' }</pre>
			{ this.props.children }
			{ lockouts.length === 0 ? <></> : <Spoiler summary="Lockouts">
				{lockouts.map( ( { username, expires, turns }, i ) => (
					<div key={`d${i}`}>
						<b>{username}</b>
						{ ' is locked out for ' }
						{[
							turns === 1 ? `${turns} turn or `
						:	turns > 1 ? `${turns} turns or `
						:	'',
							( duration( expires - now, 'ms' ) as any ).format( 'h [hours], m [minutes], s [seconds]', { trim: 'all' } )
						].filter( s => !!s )}.
					</div>
				) )}</Spoiler> }
		</div>;
	}
}
